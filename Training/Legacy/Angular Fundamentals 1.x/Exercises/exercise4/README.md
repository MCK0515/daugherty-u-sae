# DBS SA&E Angular Exercises

## Exercise 4

### Angular Concepts

* Use an angular service method that returns a promise
* Utilize angular's eventing (publish/subscribe) mechanism to update component data from a central source

### Details

Enhance the `login` method on the `HomeCtrl` to use the `AuthSvc.login` service method and show logged-in user in the header by 
subscribing to the `AuthSvc.LOGGED_IN` event via `AuthSvc.subscribe` with the `$scope` of the `HeaderCtrl` and binding the user
information in `header.html`.

### Web Resources

* https://toddmotto.com/all-about-angulars-emit-broadcast-on-publish-subscribing/
* http://www.dwmkerr.com/promises-in-angularjs-the-definitive-guide/

