class AuthSvc {

  constructor($rootScope, toastr, $q) {
    this.$rootScope = $rootScope;
    this.toastr = toastr;
    this.$q = $q;

    this.EVENTS = {
      LOGGED_IN: 'loggedIn'
    };
    this.user = null;
  }

  subscribe(scope, event, callback) {
    scope.$on(event, callback);
  }

  broadcast(event, value) {
    this.$rootScope.$broadcast(event, value);
  }

  login(username, password) {
    if (username && password) {
      return this.$q.resolve().then(() => {
        this.user = username;
        this.broadcast(this.EVENTS.LOGGED_IN, this.user);
        return username;
      });
    }
    else {
      return this.$q.reject();
    }
  }
}

export default AuthSvc;