class HomeCtrl {

  constructor(toastr) {
    this.user = {
      username: '',
      password: ''
    };
    this.toastr = toastr;
  }

  login() {
    // TODO: login user using authService
    if (this.user.username && this.user.password) {
      this.loggedInUser = this.user.username;
    }
    else {
      this.toastr.error('Failed to login user!');
    }
  }
}

export default HomeCtrl;