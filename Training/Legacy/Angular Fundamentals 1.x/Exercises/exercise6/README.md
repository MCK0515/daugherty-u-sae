# DBS SA&E Angular Exercises

## Exercise 6

### Angular Concepts

* Using angular $http to make real AJAX request

### Details

Allow a logged-in user to logout via a button in `header.html` and a subsequent action in `HeaderCtrl` that utilizes a new method,
`AuthSvc.logout`, which should make an AJAX request to `POST /logout` and upon success, remove the previously stored user from localStorage via
`localStorageSvc.removeItem`. Afterward, the user should see the login page and user information removed from the header.
