class HomeCtrl {

  constructor($scope, toastr, AuthSvc) {
    this.user = {
      username: '',
      password: ''
    };
    this.$scope = $scope;
    this.toastr = toastr;
    this.AuthSvc = AuthSvc;

    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_IN, (event, user) => {
      this.loggedInUser = user;
    });
  }

  login() {
    this.AuthSvc.login(this.user.username, this.user.password).then(response => {
      this.loggedInUser = response;
    }, error => {
      this.toastr.error('Failed to login user!');
    });
  }
}

export default HomeCtrl;