class AuthSvc {

  constructor($rootScope, toastr, $q, $http, LocalStorageSvc) {
    this.$rootScope = $rootScope;
    this.toastr = toastr;
    this.$q = $q;
    this.$http = $http;
    this.LocalStorageSvc = LocalStorageSvc;
    this.EVENTS = {
      LOGGED_IN: 'loggedIn'
    };

    this.user = this.LocalStorageSvc.getItem('user') ? JSON.parse(this.LocalStorageSvc.getItem('user')) : null;
    if (this.user) {
      this.broadcast(this.EVENTS.LOGGED_IN, this.user);
    }
  }

  subscribe(scope, event, callback) {
    scope.$on(event, callback);
    if (this.user && event === this.EVENTS.LOGGED_IN) {
      callback(null, this.user);
    }
  }

  broadcast(event, value) {
    this.$rootScope.$broadcast(event, value);
  }

  login(username, password) {
    if (username && password) {
      return this.$http.post('/login', { username: username, password: password }).then((response) => {
        this.user = response.data;
        this.broadcast(this.EVENTS.LOGGED_IN, this.user);
        this.LocalStorageSvc.setItem('user', JSON.stringify(this.user));
        return this.user;
      });
    }
    else {
      return this.$q.reject();
    }
  }
}

export default AuthSvc;