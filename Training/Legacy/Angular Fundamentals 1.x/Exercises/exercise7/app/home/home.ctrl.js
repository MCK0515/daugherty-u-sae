class HomeCtrl {

  constructor($scope, toastr, AuthSvc) {
    this.user = {
      username: '',
      password: ''
    };
    this.$scope = $scope;
    this.toastr = toastr;
    this.AuthSvc = AuthSvc;

    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_IN, (event, user) => {
      this.loggedInUser = user;
    });
    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_OUT, () => {
      this.loggedInUser = null;
    });
  }

  login() {
    this.AuthSvc.login(this.user.username, this.user.password).then(response => {
      this.loggedInUser = response;
      // TODO: route to user page using $state go
    }, error => {
      this.toastr.error('Failed to login user!');
    });
  }
}

export default HomeCtrl;