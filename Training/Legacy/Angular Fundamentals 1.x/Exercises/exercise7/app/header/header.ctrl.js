
class HeaderCtrl {
  constructor($scope, AuthSvc) {
    this.$scope = $scope;
    this.AuthSvc = AuthSvc;
  }

  logout() {
    this.AuthSvc.logout();
  }

  $onInit() {
    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_IN, (event, user) => {
      this.user = user;
    });

    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_OUT, () => {
      this.user = null;
    });
  }
}

export default HeaderCtrl;