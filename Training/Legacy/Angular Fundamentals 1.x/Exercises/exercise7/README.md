# DBS SA&E Angular Exercises

## Exercise 7

### Angular Concepts

* Creating a new state (route)
### Details

Enhance login even further by taking the user to a new page showing all users in the system upon successful login. 
To do this, create a new directory under `app` called `user` and within create `UserCtl` and `user.html`. 
You will also need to update the state definitions in `app.js` with a new `User` state.

### Web Resources

* http://angular-ui.github.io/ui-router/site/#/api/ui.router.state.$stateProvider

