# DBS SA&E Angular Exercises

## Exercise 12

### Angular Concepts

* ng-submit
* angular form validation

### Details

Add the ability to create a new user. Add a button to the users page which, when clicked, takes the user to a new
create user page. You will need to create a new state `UserCreate` and wire it up to a new controller, `UserCreateCtrl` and
a new template `user-create.html`. 

On this page you should capture all user information:

* firstName
* lastName
* username
* birthdate
* gender
* email
* phone
* address
    * line1
    * line2
    * city
    * state
    * zip

Using the default HTML5 `required` attribute, ensure the form is valid via angular before saving the user.
 
Enhance the `UserSvc` to allow the creation of a user by utilizing the RESTful API `POST /users` and providing the user object as the body of the request.
The API will return the saved user, populated with a unique id.

Once the new user has been successfully saved, navigate back to the Users page with the up-to-date users data.

### Web Resources

* https://docs.angularjs.org/api/ng/directive/ngSubmit
* https://docs.angularjs.org/guide/forms
* https://docs.angularjs.org/api/ng/service/$http#post

