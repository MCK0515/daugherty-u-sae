class HomeCtrl {
  //TODO: Inject toastr
  constructor($scope) {
    $scope.user = {
      username: '',
      password: ''
    };
    $scope.loggedInUser = null;

    $scope.login = function() {
      if ($scope.user.username && $scope.user.password) {
        $scope.loggedInUser = $scope.user.username;
      }
      else {
        //TODO: replace console.log to toastr.error
        console.log('Failed to login user!');
      }
    };
  }
}

export default HomeCtrl;