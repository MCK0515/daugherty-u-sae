# DBS SA&E Angular Exercises

## Exercise 2

### Angular Concepts

* Dependency Injection (DI) of 3rd party service

### Details

The application has a dependency on the angular-toastr package. This package allows for us to inject the `toastr` service
into angular controllers, services, factories, directives, components, etc. Using the `toastr` service in HomeCtrl to replace
the `console.log` error handling you wrote in exercise 1 by invoking `toastr.error(msg)` where `msg` is the error message you
previously wrote in `console.log`. If implemented successfully, upon an error you should see a red message pop out of the top 
right side of the page.