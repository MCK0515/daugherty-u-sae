import 'angular';
import 'angular-ui-router';
import 'lodash';
require('bootstrap-loader');
import 'angular-toastr';
import 'angular-toastr/dist/angular-toastr.css';
require('font-awesome/css/font-awesome.css');

import HeaderCtrl from './header/header.ctrl.js';
import HeaderComp from './header/header.comp.js';
import FooterCtrl from './footer/footer.ctrl.js';
import FooterComp from './footer/footer.comp.js';
import HomeCtrl from './home/home.ctrl.js';

let module = angular.module('DBSBootcamp', ['ui.router', 'toastr'])
    .controller('HeaderCtrl', HeaderCtrl)
    .controller('FooterCtrl', FooterCtrl)
    .component('dbsHeader', HeaderComp)
    .component('dbsFooter', FooterComp)
    .controller('HomeCtrl', HomeCtrl);

module.config(($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/');

  $stateProvider
      .state('Home', {
        url: '/',
        // TODO: use "controller as" syntax
        controller: 'HomeCtrl',
        template: require('./home/home.html')
      });
});

module.config(($httpProvider) => {
  $httpProvider.interceptors.push(() => {
    return {
      request: (httpConfig) => {
        if(!~httpConfig.url.indexOf('.html')) {
          httpConfig.url = `/api${httpConfig.url}`;
        }

        return httpConfig;
      }
    }
  });
});