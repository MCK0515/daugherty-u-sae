class HomeCtrl {
  // TODO: convert to use controller as instead of $scope
  constructor($scope, toastr) {
    $scope.user = {
      username: '',
      password: ''
    };
    $scope.loggedInUser = null;

    $scope.login = function() {
      if ($scope.user.username && $scope.user.password) {
        $scope.loggedInUser = $scope.user.username;
      }
      else {
        toastr.error('Failed to login user!');
      }
    };
  }
}

export default HomeCtrl;