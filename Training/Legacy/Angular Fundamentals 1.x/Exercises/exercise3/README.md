# DBS SA&E Angular Exercises

## Exercise 3

### Angular Concepts

* Best Practice: "Controller as" syntax

### Details

Angular provides a way to use controllers as regular JavaScript objects or classes by treating that object/class itself "as" a `$scope` variable
rather than working with the `$scope` directly. Modify the HomeCtrl to use the "controller as" syntax by setting functions and properties of the 
controller class itself rather than on `$scope`. You will also need to update the syntax used in the state definitions in `app.js` and the bindings 
previously setup in `home.html`

### Web Resources

* https://toddmotto.com/digging-into-angulars-controller-as-syntax/