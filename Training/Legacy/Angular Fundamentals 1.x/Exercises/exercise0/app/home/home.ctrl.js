class HomeCtrl {
  
  constructor($scope) {
    $scope.user = {
      username: '',
      password: ''
    };
    $scope.loggedInUser = null;
    
    $scope.login = function() {
      if($scope.user.username && $scope.user.password) {
        $scope.loggedInUser = $scope.user.username;
      } else {
        //toast!
      }
    };
  }
}

export default HomeCtrl;