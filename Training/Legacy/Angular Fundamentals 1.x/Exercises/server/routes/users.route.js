'use strict';

var UsersSvc = require('./../service/users.svc');
var User = require('./../domain/user');
var Address = require('./../domain/address');

module.exports = function (server, AppCache) {
  let Users = new UsersSvc(AppCache);

  // Find Many
  server.get('/users', function (req, res, next) {
    let filterProp = req.query.filterProp ? req.query.filterProp : 'id';
    let filterText = req.query.filterText ? req.query.filterText: '';
    let sortAsc = req.query.sortAsc ? (req.query.sortAsc == 'true') : true;
    let max = req.query.max ? parseInt(req.query.max) : 10;
    let offset = req.query.offset ? parseInt(req.query.offset) : 0;

    res.send(200, Users.findMany(filterProp, filterText, sortAsc, max, offset));
  });

  // Find Single
  server.get('/users/:id', function (req, res, next) {
    let user = Users.findById(parseInt(req.params.id));

    if(user) {
      res.send(200, user);
    } else {
      res.send(404);
    }
  });

  // Create
  server.post('/users', function (req, res, next) {
    let jsonUser = req.body;
    jsonUser.id = -1;
    let user = buildUserFromJson(jsonUser);

    try {
      user = Users.create(user);
      res.send(201, user);
    } catch(e) {
      res.send(422, e.message);
    }
  });

  // Update
  server.put('/users/:id', function (req, res, next) {
    let jsonUser = req.body;
    let user = buildUserFromJson(jsonUser);

    try {
      user = Users.update(user);
      res.send(200, user);
    } catch(e) {
      res.send(422, e.message);
    }
  });

  // Delete
  server.del('/users/:id', function (req, res, next) {
    try {
      Users.remove(parseInt(req.params.id));
      res.send(204);
    } catch(e) {
      res.send(400, e.message);
    }
  });

  function buildUserFromJson(jsonUser) {
    return new User(parseInt(jsonUser.id), jsonUser.username, jsonUser.firstName, jsonUser.lastName, jsonUser.active,
      jsonUser.email, jsonUser.phone, jsonUser.gender, jsonUser.dob, new Address(jsonUser.address.line1,
        jsonUser.address.line2, jsonUser.address.city, jsonUser.address.state, jsonUser.address.zip));
  }
};
