'use strict';

var UsersSvc = require('./../service/users.svc');

module.exports = function (server, AppCache) {
  let Users = new UsersSvc(AppCache);

  server.post('/login', function (req, res, next) {
    var credentials = req.body;
    if(credentials.username && credentials.password) {
      let user = Users.findByUsername(credentials.username);
      if(user) {
        AppCache.set('loggedInUser', user);
        res.send(200, user);
      } else {
        res.send(404, {message: 'User not found'});
      }
    } else {
      res.send(422, {message: 'Username and password are required'});
    }
  });

  server.post('/logout', function (req, res, next) {
    AppCache.del('loggedInUser');
    res.send(204);
  });
};
