'use strict';

var _ = require('lodash');

class User {
  constructor(id, username, firstName, lastName, active, email, phone, gender, dob, address) {
    this.id = id;
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.active = active;
    this.email = email;
    this.phone = phone;
    this.gender = gender;
    this.dob = dob;
    this.address = address;
  }

  isValid() {
    return !!this.username && !!this.firstName && !!this.lastName && !!this.email && this.phoneIsValid() &&
      this.genderIsValid() && !!this.dob && this.address.isValid()
  }

  phoneIsValid() {
    let valid = false;
    let phoneParts = _.split(this.phone, '-');
    if(phoneParts.length === 3) {
      if(_.filter(_.flatten(phoneParts), (part) => { return isNaN(part); }).length === 0) {
        valid = phoneParts[0].length === 3 &&
          phoneParts[1].length === 3 && phoneParts[2].length === 4;
      }
    }

    return valid;
  }

  genderIsValid() {
    return this.gender === 'M' || this.gender === 'F';
  }
}

module.exports = User;
