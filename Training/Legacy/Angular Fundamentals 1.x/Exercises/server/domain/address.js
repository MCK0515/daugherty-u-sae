'use strict';

class Address {
  constructor(line1, line2, city, state, zip) {
    this.line1 = line1;
    this.line2 = line2;
    this.city = city;
    this.state = state;
    this.zip = zip;
  }

  isValid() {
    return !!this.line1 && !!this.city && !!this.state && !!this.zip;
  }
}

module.exports = Address;
