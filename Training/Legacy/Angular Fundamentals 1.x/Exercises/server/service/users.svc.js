'use strict';

var _ = require('lodash');

class UsersSvc {
  constructor(AppCache) {
    this.AppCache = AppCache;
  }

  findById(id) {
    let users = this.AppCache.get('users');
    return _.find(users, {id: id});
  }

  findByUsername(username) {
    let users = this.AppCache.get('users');
    return _.find(users, {'username': username});
  }

  findMany(filterProp, filterText, sortAsc, max, offset) {
    let users = this.AppCache.get('users');
    users = _.sortBy(users, filterProp);

    if(!sortAsc) {
      _.reverse(users);
    }

    if(filterText) {
      users = _.filter(users, (user) => {
        if(filterProp === 'id') {
          return parseInt(user[filterProp]) === parseInt(filterText);
        } else {
          return ~user[filterProp].toLowerCase().indexOf(filterText.toLowerCase());
        }
      });
    }

    let endPosition = offset + max;
    if(endPosition > users.length) {
      endPosition = users.length;
    }
    users = _.slice(users, offset, endPosition);

    return users;
  }

  create(user) {
    if(user.isValid()) {
      let users = this.AppCache.get('users');
      let largestUserId = _.last(_.sortBy(users, 'id')).id;
      user.id = largestUserId + 1;
      users.push(user);
      this.AppCache.set('users', users);
      return user;
    } else {
      throw new Error('User is not valid');
    }
  }

  update(user) {
    if(user.id >= 0 && user.isValid()) {
      let users = this.AppCache.get('users');
      let userIndex = _.findIndex(users, {id: user.id});
      if(userIndex < 0) {
        throw new Error('Cannot update user that does not exist');
      }
      users.splice(userIndex, 1, user);
      this.AppCache.set('users', users);
      return user;
    }

    throw new Error('User is not valid');
  }

  remove(id) {
    let users = this.AppCache.get('users');
    let userIndex = _.findIndex(users, {id: id});
    if(userIndex < 0) {
      throw new Error('Cannot delete user that does not exist');
    }

    _.pullAt(users, userIndex);
    this.AppCache.set('users', users);
  }
}

module.exports = UsersSvc;
