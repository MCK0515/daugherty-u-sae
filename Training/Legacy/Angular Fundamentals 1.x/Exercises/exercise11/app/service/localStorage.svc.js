class LocalStorageSvc {

  constructor($window) {
    this.localStorage = $window.localStorage;
  }

  getItem(key) {
    return this.localStorage.getItem(key);
  }

  setItem(key, value) {
    this.localStorage.setItem(key, value);
  }

  removeItem(key) {
    this.localStorage.removeItem(key);
  }

  clear() {
    this.localStorage.clear();
  }
}

export default LocalStorageSvc;