class UserSvc {

  constructor($http) {
    this.$http = $http;
  }

  getUsers(filterParams) {
    if(angular.isUndefined(filterParams)) {
      filterParams = {};
    }
    return this.$http.get('/users', { params: filterParams }).then(response => {
      return response.data;
    });
  }
}

export default UserSvc;