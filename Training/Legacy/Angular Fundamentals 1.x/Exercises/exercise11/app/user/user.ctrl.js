class UserCtrl {

  constructor(users, UserSvc, toastr) {
    this.users = users;
    this.UserSvc = UserSvc;
    this.toastr = toastr;
    this.searchBy = "id";
    this.searchFor = null;
    this.sortAsc = true;
  }

  search() {
    var params = {
      filterProp: this.searchBy,
      filterText: this.searchFor,
      sortAsc: this.sortAsc
    };

    this.UserSvc.getUsers(params).then(response => {
      this.users = response;
    }, error => {
      this.toastr.error('Failed to get users');
    });
  }
}

export default UserCtrl;