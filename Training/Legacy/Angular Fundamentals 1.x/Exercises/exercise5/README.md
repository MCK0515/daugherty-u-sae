# DBS SA&E Angular Exercises

## Exercise 5

### Angular Concepts

* Using angular $http to make real AJAX request
* Store logged-in user for client-side caching to prevent re-login when refreshing the page

### Details

Enhance `AuthSvc.login` to use a real AJAX call to `POST /login` with a body of an object containing the `username` and `password` fields.
You must use the username of one of the users in the server's `seed-data.js` file for successful login. Password only need be non-empty.
After successful login, the API will return to you the the user object of the user who was logged in. If the user does not exist, the API
will return a 404 error. For this exercise, you can use the username `test`. For all exercises, the password does not matter - it just needs
some value. After receiving user data, store the logged-in user in the browser localStorage via `localStorageSvc.setItem` so that the user
is auto logged-in on page refresh

### Web Resources

* https://docs.angularjs.org/api/ng/service/$http

