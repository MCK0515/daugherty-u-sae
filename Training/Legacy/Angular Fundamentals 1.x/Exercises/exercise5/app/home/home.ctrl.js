class HomeCtrl {

  constructor(toastr, AuthSvc) {
    this.user = {
      username: '',
      password: ''
    };
    this.toastr = toastr;
    this.AuthSvc = AuthSvc;
    // TODO: call AuthSvc to set loggedInUser if user exists in local storage
  }

  login() {
    this.AuthSvc.login(this.user.username, this.user.password).then(response => {
      this.loggedInUser = response;
    }, error => {
      this.toastr.error('Failed to login user!');
    });
  }
}

export default HomeCtrl;