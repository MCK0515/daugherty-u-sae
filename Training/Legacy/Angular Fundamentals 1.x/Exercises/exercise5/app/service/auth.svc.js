class AuthSvc {

  constructor($rootScope, toastr, $q) {
    this.$rootScope = $rootScope;
    this.toastr = toastr;
    this.$q = $q;
    
    this.EVENTS = {
      LOGGED_IN: 'loggedIn'
    };
    // TODO: use local storage to set user if user exists
    this.user = null;
  }

  subscribe(scope, event, callback) {
    scope.$on(event, callback);
  }

  broadcast(event, value) {
    this.$rootScope.$broadcast(event, value);
  }

  login(username, password) {
    if (username && password) {
      // TODO: use $http instead to call /login
      return this.$q.resolve().then(() => {
        this.user = username;
        this.broadcast(this.EVENTS.LOGGED_IN, this.user);
        return username;
      });
    }
    else {
      return this.$q.reject();
    }
  }
}

export default AuthSvc;