
class HeaderCtrl {
  constructor($scope, AuthSvc) {
    this.$scope = $scope;
    this.AuthSvc = AuthSvc;
  }

  $onInit() {
    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_IN, (event, user) => {
      this.user = user;
    });
  }
}

export default HeaderCtrl;