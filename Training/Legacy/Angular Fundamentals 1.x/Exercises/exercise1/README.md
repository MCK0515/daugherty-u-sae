# DBS SA&E Angular Exercises

## Exercise 1

### Angular Concepts

* Binding variables
* Event Handling
* Built-in directives

### Details

You have been provided a HomeCtrl and its template (home.html). HomeCtrl contains a variable `$scope.user` which contains a `username` and `password` field 
that should be bound to the login form input fields in home.html using ng-model.
You have also been provided a function `$scope.login` on HomeCtrl which should be used as the login form submit handler via ng-submit. This login function should 
validate that username and password are not empty. Once validated, set `$scope.loggedInUser` to provided username and use the built-in angular directives 
`ng-show`/`ng-hide` or `ng-if` to show the loggedInUser on the page and hide the login form. If the validation fails, use `console.log(msg)` where `msg` is a string indicating the error.