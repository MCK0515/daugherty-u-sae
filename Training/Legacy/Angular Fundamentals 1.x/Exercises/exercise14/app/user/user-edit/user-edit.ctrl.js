class UserEditCtrl {

  constructor(UserSvc, $state, toastr, $filter, user) {
    this.UserSvc = UserSvc;
    this.$state = $state;
    this.toastr = toastr;

    user.dob = $filter('date')(user.dob, 'MM/dd/yyyy');
    this.user = user;
  }

  updateUser(userEditFrm) {
    if (userEditFrm.$valid) {
      this.UserSvc.updateUser(this.user).then(() => {
        this.$state.go('User');
      }, (error) => {
        this.toastr.error(error.data);
      });
    }
  }

  cancel() {
    this.$state.go('User');
  }
}

export default UserEditCtrl;