# DBS SA&E Angular Exercises

## Exercise 14

### Angular Concepts

* $http delete

### Details

Add the ability for deleting a user on the Users page.

Enhance `UserSvc` to handle deleting a user. You should use the RESTful API `DELETE /users/{id}`. Upon successful
delete of the user, reload the Users page with the updated user list.


### Web Resources

* https://docs.angularjs.org/api/ng/service/$http#delete

