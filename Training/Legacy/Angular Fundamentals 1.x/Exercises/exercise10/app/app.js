import 'angular';
import 'angular-ui-router';
import 'lodash';
require('bootstrap-loader');
import 'angular-toastr';
import 'angular-toastr/dist/angular-toastr.css';
require('font-awesome/css/font-awesome.css');

import HeaderCtrl from './header/header.ctrl.js';
import HeaderComp from './header/header.comp.js';
import FooterCtrl from './footer/footer.ctrl.js';
import FooterComp from './footer/footer.comp.js';
import HomeCtrl from './home/home.ctrl.js';
import AuthSvc from './service/auth.svc.js';
import LocalStorageSvc from './service/localStorage.svc.js';
import UserCtrl from './user/user.ctrl.js';
import UserSvc from './user/user.svc.js';

let module = angular.module('DBSBootcamp', ['ui.router', 'toastr'])
    .controller('HeaderCtrl', HeaderCtrl)
    .controller('FooterCtrl', FooterCtrl)
    .component('dbsHeader', HeaderComp)
    .component('dbsFooter', FooterComp)
    .controller('HomeCtrl', HomeCtrl)
    .service('AuthSvc', AuthSvc)
    .service('LocalStorageSvc', LocalStorageSvc)
    .controller('UserCtrl', UserCtrl)
    .service('UserSvc', UserSvc);

module.config(($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/');

  $stateProvider
      .state('Home', {
        url: '/',
        controller: 'HomeCtrl as ctrl',
        template: require('./home/home.html')
      })
      .state('User', {
        url: '/users',
        controller: 'UserCtrl as ctrl',
        template: require('./user/user.html'),
        resolve: {
          users: function(UserSvc) {
            return UserSvc.getUsers();
          }
        }
      });
});

module.config(($httpProvider) => {
  $httpProvider.interceptors.push(() => {
    return {
      request: (httpConfig) => {
        if(!~httpConfig.url.indexOf('.html')) {
          httpConfig.url = `/api${httpConfig.url}`;
        }

        return httpConfig;
      }
    }
  });
});