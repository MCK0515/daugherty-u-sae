class UserSvc {

  constructor($http) {
    this.$http = $http;
  }

  getUsers() {
    return this.$http.get('/users').then(response => {
      return response.data;
    });
  }
}

export default UserSvc;