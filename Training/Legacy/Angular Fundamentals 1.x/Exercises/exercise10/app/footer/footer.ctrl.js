class FooterCtrl {
  get currentYear() {
    return (new Date()).getFullYear();
  }
}

export default FooterCtrl;