# DBS SA&E Angular Exercises

## Exercise 9

### Angular Concepts

* ng-repeat
* Injection of resolved variables

### Details

Display all users on the page utilizing an API call to `GET /users` from a new `UserSvc.getUsers` method from the `resolve` 
block in the `$stateProvider` in `app.js` and inject the resolved variables `users` into the `UserCtrl`. Display the users in a table
with the following attributes: `id`, `username`, `firstName`, `lastName`, `email`, `phone`, and `dob`

### Web Resources

* http://angular-ui.github.io/ui-router/site/#/api/ui.router.state.$stateProvider

