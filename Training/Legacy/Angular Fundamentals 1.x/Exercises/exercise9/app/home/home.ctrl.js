class HomeCtrl {

  constructor($scope, toastr, AuthSvc, $state) {
    this.user = {
      username: '',
      password: ''
    };
    this.$scope = $scope;
    this.toastr = toastr;
    this.AuthSvc = AuthSvc;
    this.$state = $state;

    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_IN, (event, user) => {
      this.loggedInUser = user;
    });
    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_OUT, () => {
      this.loggedInUser = null;
    });
  }

  login() {
    this.AuthSvc.login(this.user.username, this.user.password).then(response => {
      this.loggedInUser = response;
      this.$state.go('User');
    }, error => {
      this.toastr.error('Failed to login user!');
    });
  }
}

export default HomeCtrl;