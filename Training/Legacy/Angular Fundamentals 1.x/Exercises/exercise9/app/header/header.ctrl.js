
class HeaderCtrl {
  constructor($scope, AuthSvc, $state) {
    this.$scope = $scope;
    this.AuthSvc = AuthSvc;
    this.$state = $state;
  }

  logout() {
    this.AuthSvc.logout();
  }

  $onInit() {
    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_IN, (event, user) => {
      this.user = user;
    });

    this.AuthSvc.subscribe(this.$scope, this.AuthSvc.EVENTS.LOGGED_OUT, () => {
      this.user = null;
      this.$state.go('Home');
    });
  }
}

export default HeaderCtrl;