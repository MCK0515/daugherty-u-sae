# DBS SA&E Angular Exercises

## Exercise 15

### Angular Concepts

* ng-class
* ng-style

### Details

Add the ability to sort users by column headers on the Users page to replace the dropdown and checkbox. Distinguish the active column header by using `ng-style` 
and set the `color` to `red`. Based on order by, use `ng-class` to display `fa-caret-up` for ascending and `fa-caret-down` 
for descending on the active column. During a user search, use the active column for filtering search results. 

### Web Resources

* https://docs.angularjs.org/api/ng/directive/ngClass
* https://scotch.io/tutorials/the-many-ways-to-use-ngclass
* https://docs.angularjs.org/api/ng/directive/ngStyle

