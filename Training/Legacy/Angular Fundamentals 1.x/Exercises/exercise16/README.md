# DBS SA&E Angular Exercises

## Exercise 16

### Angular Concepts

* directives
* transclusion

### Details

Covert sortable headers into a reusable angular attribute directive using transclusion. You should use isolate scope.
You will likely need to use various scope bindings for your solution. The github gist below may help.

### Web Resources

* https://www.sitepoint.com/practical-guide-angularjs-directives/
* https://www.sitepoint.com/practical-guide-angularjs-directives-part-two/ (scoping + transclusion)
* https://gist.github.com/CMCDragonkai/6282750

