import 'angular';
import 'angular-ui-router';
import 'lodash';
require('bootstrap-loader');
import 'angular-toastr';
import 'angular-toastr/dist/angular-toastr.css';
require('font-awesome/css/font-awesome.css');

import HeaderCtrl from './header/header.ctrl.js';
import HeaderComp from './header/header.comp.js';
import FooterCtrl from './footer/footer.ctrl.js';
import FooterComp from './footer/footer.comp.js';
import HomeCtrl from './home/home.ctrl.js';
import AuthSvc from './service/auth.svc.js';
import LocalStorageSvc from './service/localStorage.svc.js';
import UserCtrl from './user/user.ctrl.js';
import UserSvc from './user/user.svc.js';
import UserViewCtrl from './user/user-view/user-view.ctrl.js';
import UserCreateCtrl from './user/user-create/user-create.ctrl.js';
import UserEditCtrl from './user/user-edit/user-edit.ctrl.js';

//TODO: Import directive function and wrap with angular: module.directive('sortableHeader', SortableHeaderDir)

let module = angular.module('DBSBootcamp', ['ui.router', 'toastr'])
    .controller('HeaderCtrl', HeaderCtrl)
    .controller('FooterCtrl', FooterCtrl)
    .component('dbsHeader', HeaderComp)
    .component('dbsFooter', FooterComp)
    .controller('HomeCtrl', HomeCtrl)
    .service('AuthSvc', AuthSvc)
    .service('LocalStorageSvc', LocalStorageSvc)
    .controller('UserCtrl', UserCtrl)
    .service('UserSvc', UserSvc)
    .controller('UserViewCtrl', UserViewCtrl)
    .controller('UserCreateCtrl', UserCreateCtrl)
    .controller('UserEditCtrl', UserEditCtrl);

module.config(($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/');

  $stateProvider
      .state('Home', {
        url: '/',
        controller: 'HomeCtrl as ctrl',
        template: require('./home/home.html')
      })
      .state('User', {
        url: '/users',
        controller: 'UserCtrl as ctrl',
        template: require('./user/user.html'),
        resolve: {
          users: function(UserSvc) {
            return UserSvc.getUsers();
          }
        }
      })
      .state('UserCreate', {
        url: '/users/create',
        controller: 'UserCreateCtrl as ctrl',
        template: require('./user/user-create/user-create.html')
      })
      .state('UserView', {
        url: '/users/:id',
        controller: 'UserViewCtrl as ctrl',
        template: require('./user/user-view/user-view.html'),
        resolve: {
          user: function(UserSvc, $stateParams) {
            return UserSvc.getUserById($stateParams.id);
          }
        }
      })
      .state('UserEdit', {
        url: '/users/:id/edit',
        controller: 'UserEditCtrl as ctrl',
        template: require('./user/user-edit/user-edit.html'),
        resolve: {
          user: function(UserSvc, $stateParams) {
            return UserSvc.getUserById($stateParams.id);
          }
        }
      });
});

module.run(['$templateCache', function($templateCache) {
  var url = './user/user-editor.html';
  $templateCache.put(url, require(url));
}]);

module.config(($httpProvider) => {
  $httpProvider.interceptors.push(() => {
    return {
      request: (httpConfig) => {
        if(!~httpConfig.url.indexOf('.html')) {
          httpConfig.url = `/api${httpConfig.url}`;
        }

        return httpConfig;
      }
    }
  });
});