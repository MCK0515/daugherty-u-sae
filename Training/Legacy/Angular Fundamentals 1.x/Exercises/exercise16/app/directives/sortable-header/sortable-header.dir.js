function sortableHeaderDir () {
  return {
    template: require('./sortable-header.html'),
    controller: 'SortableHeaderCtrl as ctrl',
    bindToController: true,
    transclude: true,
    restrict: 'A',
    scope: {
      //TODO: Add directive scope variables
    }
  };
}

export default sortableHeaderDir;