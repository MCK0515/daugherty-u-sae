class UserCtrl {

  constructor(users, UserSvc, toastr, $state) {
    this.users = users;
    this.UserSvc = UserSvc;
    this.toastr = toastr;
    this.$state = $state;

    this.searchBy = "id";
    this.searchFor = null;
    this.sortAsc = true;
  }

  search() {
    var params = {
      filterProp: this.searchBy,
      filterText: this.searchFor,
      sortAsc: this.sortAsc
    };

    return this.UserSvc.getUsers(params).then(response => {
      this.users = response;
    }, () => {
      this.toastr.error('Failed to get users');
    });
  }

  deleteUser(user) {
    if(confirm(`Are you sure you want to delete ${user.username}?`)) {
      this.UserSvc.deleteUser(user.id)
          .then(this.search.bind(this))
          .catch(() => {
            this.toastr.error('Failed to delete user');
          });
    }
  }

  changeSearchBy(property) {
    if(this.searchBy !== property) {
      this.searchBy = property;
      this.sortAsc = true;
    } else {
      this.sortAsc = !this.sortAsc;
    }

    this.search();
  }

  isActiveFilter(property) {
    return property === this.searchBy;
  }
}

export default UserCtrl;