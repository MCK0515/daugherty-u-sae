# DBS SA&E Angular Exercises

## Exercise 8

### Angular Concepts

* Adding a hyperlink with ui-router
* Conditional display using ng-if/ng-show

### Details

Add a hyperlink to the Users page in the header menu and conditionally show it only if a user has been authenticated using `ng-if or ng-show`

### Web Resources

* http://angular-ui.github.io/ui-router/site/#/api/ui.router.state.directive:ui-sref

