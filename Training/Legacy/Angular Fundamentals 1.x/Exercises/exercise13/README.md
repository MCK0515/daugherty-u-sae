# DBS SA&E Angular Exercises

## Exercise 13

### Angular Concepts

* ng-include

### Details

Add the ability for editing a user. You will need to create a new page/state `UserEdit` and wire it up to a new controller, `UserEditCtrl` and
template, `user-edit.html`. Because creating and editing a user modify the same user properties, make use of `ng-include` to share the form fields between both
create and edit user templates with the `./user/user-editor.html` template that has been stubbed for you.

You will need to add a way to get to the edit user page from both the Users page and the View User page.

Enhance `UserSvc` to handle updating a user. You should use the RESTful API `PUT /users/{id}` with the user as the body of the request. Upon successful
update of the user, redirect to the Users page with the updated user list.

### Web Resources

* https://docs.angularjs.org/api/ng/directive/ngInclude
* https://docs.angularjs.org/api/ng/service/$http#put

