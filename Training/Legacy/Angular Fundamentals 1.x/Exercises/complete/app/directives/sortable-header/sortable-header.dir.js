function sortableHeaderDir () {
  return {
    template: require('./sortable-header.html'),
    controller: 'SortableHeaderCtrl as ctrl',
    bindToController: true,
    transclude: true,
    restrict: 'A',
    scope: {
      property: '@',
      searchBy: '=',
      sortAsc: '=',
      search: '&'
    }
  };
}

export default sortableHeaderDir;