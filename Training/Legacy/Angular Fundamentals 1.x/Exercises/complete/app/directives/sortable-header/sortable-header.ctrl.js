class SortableHeaderCtrl {
  constructor($timeout) {
    this.$timeout = $timeout
  }

  isActive() {
    return this.property === this.searchBy;
  }

  changeSearchBy() {
    if(this.searchBy !== this.property) {
      this.searchBy = this.property;
      this.sortAsc = true;
    } else {
      this.sortAsc = !this.sortAsc;
    }

    this.$timeout(this.search);
  }
}

export default SortableHeaderCtrl;