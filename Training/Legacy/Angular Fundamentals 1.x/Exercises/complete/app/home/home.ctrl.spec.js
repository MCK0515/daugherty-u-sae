//import './../app';

describe('HomeCtrl', () => {
  let HomeCtrl;

  beforeEach(() => {
    angular.mock.module('DBSBootcamp');
    angular.mock.inject(($controller, toastr) => {
      HomeCtrl = $controller('HomeCtrl', {
        toastr: toastr
      });
    });
  });

  it('sets properties on construction', () => {
    expect(HomeCtrl.user).not.toBeNull();
    expect(HomeCtrl.user.username).toEqual('');
    expect(HomeCtrl.user.password).toEqual('');
    expect(HomeCtrl.loggedInUser).toBeNull();
  });
});