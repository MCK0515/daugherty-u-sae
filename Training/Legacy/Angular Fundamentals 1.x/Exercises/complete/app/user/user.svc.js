class UserSvc {

  constructor($http) {
    this.$http = $http;
  }

  getUsers(filterParams) {
    if(angular.isUndefined(filterParams)) {
      filterParams = {};
    }
    return this.$http.get('/users', { params: filterParams }).then(response => {
      return response.data;
    });
  }

  getUserById(id) {
    return this.$http.get(`/users/${id}`).then(response => {
      return response.data;
    });
  }

  createUser(user) {
    return this.$http.post('/users', user).then(response => {
      return response.data;
    })
  }

  updateUser(user) {
    return this.$http.put(`/users/${user.id}`, user).then(response => {
      return response.data;
    })
  }

  deleteUser(id) {
    return this.$http.delete(`/users/${id}`).then(response => {
      return response.data;
    });
  }
}

export default UserSvc;