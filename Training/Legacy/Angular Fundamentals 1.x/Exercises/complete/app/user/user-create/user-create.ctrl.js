class UserCreateCtrl {

  constructor(UserSvc, $state, toastr) {
    this.UserSvc = UserSvc;
    this.$state = $state;
    this.toastr = toastr;

    this.user = {
      firstName: null,
      lastName: null,
      username: null,
      gender: null,
      dob: null,
      email: null,
      phone: null,
      active: true,
      address: {
        line1: null,
        line2: null,
        city: null,
        state: null,
        zip: null
      }
    };
  }

  createUser(userCreateFrm) {
    if (userCreateFrm.$valid) {
      this.UserSvc.createUser(this.user).then(() => {
        this.$state.go('User');
      }, (error) => {
        this.toastr.error(error.data);
      });
    }
  }
}

export default UserCreateCtrl;