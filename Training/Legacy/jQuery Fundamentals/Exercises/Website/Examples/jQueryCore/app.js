﻿
// Ready function used to perform an action after the pages is loaded
$(function () {

    var div1Text = $('#myId').text();
    var div2Text = $('.myClass').text();
    var spanText = $('span').text();
    var htmlObject = $(document.body);
    
    // Create a div with some text and apend to the body element
    $('<div/>').text('Hello World!').appendTo('body');
    
});