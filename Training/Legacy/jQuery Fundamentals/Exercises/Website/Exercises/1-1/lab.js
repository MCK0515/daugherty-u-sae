﻿
var LabData = [
    {name: 'Basic Selectors',
     dom: 'dom.sample.html',
     exercises: [
        {solution: "div",
         text: "Select all div elements"},
        {solution: "img",
         text: "Select all image elements"},
        {solution: "a",
         text: "Select all hyperlinks"},
        {solution: "#coffeePot",
         text: "Select the element with id=coffeePot"},
        {solution: ".myList",
         text: "Select the elements with class=myList"},
        {solution: "span,img",
         text: "Select all span and image elements"},
     ]
    },
    {name: 'Hierarchy Selectors',
     dom: 'dom.sample.html',
     exercises: [
        {solution: "div a",
         text: "Select all hyperlinks that are descendants of a div element"},
        {solution: "div > a",
         text: "Select all hyperlinks that are the direct descendent of a div (i.e. Hyperlink3 and Hyperlink4)"},
         {solution: "div + a",
            text: "Select all hyperlinks that are 'immediately preceeded' by a sibling div. (i.e. Hyperlink1)"
         },
         {solution: "a + div",
            text: "Select divs that are 'immediately preceeded' by a sibling hyperlink. (i.e. div containing Hyperlink3 and Hyperlink4)"
         },
         {solution: "div ~ a",
            text: "Select hyperlinks that 'follow' a sibling div. (i.e. Hyperlink1 and Hyperlink2)"
         },
     ]
    },
    {name: 'Basic Filters',
     dom: 'dom.sample.html',
     exercises: [
        {solution: "a:first",
         text: "Select the first hyperlink on the page"
         },
         {solution: "a:last",
         text: "Select the last hyperlink on the page"
         },
        {solution: "a:first, a:last",
         text: "Select the first and last hyperlinks on the page"
         },
        {solution: "tbody tr:even",
         text: "Select all even rows in tbody. <br/><br/>Why does :even selector select the first,third,... rows?"},
        {solution: "img:eq(1)",
         text: "Select the second image, the dog, using the :eq(n) selector"},
        {solution: "img:nth-child(2)",
         text: "Select the second image, the dog, using the :nth-child(n) selector. How does :nth-child() differ from :eq()?"},         
        {solution: "img:not(:last)",
         text: "Select images except the last using the :not() selector"},
     ]
    },
    {name: 'Content Filters',
     dom: 'dom.sample.html',
     exercises: [
        {solution: "td:contains('Dynamic')",
         text: "Select all table cells that contain the word 'Dynamic'"},
        {solution: "tr:has(td:contains('Dynamic'))",
         text: "Use :has(selector) to select all table rows that have cells containing the word 'Dynamic'.  Hint, it's very similar to one of the previous exercises."},
        ]
    },
    {name: 'Attribute Filters',
     dom: 'dom.sample.html',
     exercises: [
      {solution: "img[alt]",
       text: "Select all images containing the 'alt' attribute"},
      {solution: "img:not([alt])",
       text: "Select all images that are missing the 'alt' attribute"},
        {solution: "a[href='#']",
         text: "Select all hyperlinks with the attribute href=#"},
        {solution: "a[href^='http']",
         text: "Select all hyperlinks where href starts with 'http'"},
        {solution: "a[href$='2']",
         text: "Select all hyperlinks where href ends with '2'"},
        {solution: "[id*='heck']",
         text: "Select all elements where id contains 'heck'"},
        {solution: "input[name][value]",
         text: "Select all input elements that have both a name and value.  What happens if you insert/remove text from the text field?"},       
     ]
    }
];

var Lab = function(parentElement) {
    this.data = LabData;
    this.exercisePosition = 0;
    this.parentElement = parentElement;
}

Lab.prototype.init = function() {
    this.exercisePosition = 0;
    this.initPageElements();
    this.updateElements();
}

Lab.prototype.initPageElements = function() {
    this.selectorInputElement = $('.labSelectorInputElement', this.parentElement);
    this.sampleDataElement = $('.labSampleDataElement', this.parentElement);
    this.sampleCodeElement = $('.labSampleCodeElement', this.parentElement);
    this.exerciseTextElement = $('.labExerciseTextElement', this.parentElement);
    this.exerciseSolutionElement = $('.labExerciseSolutionElement', this.parentElement);
    this.verifySolutionElement = $('.labVerifySolutionElement', this.parentElement);
    this.showSolutionElement = $('.labShowSolutionElement', this.parentElement);
    this.goPreviousElement = $('.labGoPreviousElement', this.parentElement);
    this.goNextElement = $('.labGoNextElement', this.parentElement);
    this.gotoElement = $('.labGotoElement', this.parentElement);
    this.errorElement = $('.labErrorElement', this.parentElement);
    this.successElement = $('.labSuccessElement', this.parentElement);
    this.progressElement = $('.labProgressElement', this.parentElement);
    this.labSelector = $('.labLabSelector', this.parentElement);
    
    this.goPreviousElement.bind('click', this, function(event) {
        event.data.goPrevious();
    });
    
    this.goNextElement.bind('click', this, function(event) {
        event.data.goNext();
    });
    
    this.gotoElement.bind('change', this, function(event) {
        event.data.gotoSelectedPosition();
    });
    
    this.verifySolutionElement.bind('click', this, function(event) {
        event.data.verifySolution();
    });
    
    this.showSolutionElement.bind('click', this, function(event) {
        event.data.showSolution();
    });
   
    this.fillLabSelector();
    var myself = this;
    $(this.labSelector).change(function() {
        myself.onLabChanged();
    });
    
    this.updateDom();
}

Lab.prototype.fillLabSelector = function() {
    for(var i = 0; i < this.data.length; i++) {
        var name = this.data[i].name;
        $(this.labSelector).append('<option>' + name + '</option>');
    }
}

Lab.prototype.updateDom = function() {
    var url = this.getCurrentDomUrl();
    
    var myself = this;
    $.get(url, function(data) {
        $(myself.sampleDataElement).html(data);
        $(myself.sampleCodeElement).html(data.replace(/</g,'&lt;').replace(/>/g,'&gt;'));
    });
    
}


Lab.prototype.onLabChanged = function() {
    this.exercisePosition = 0;
    this.updateDom();
    this.updateElements();
}

Lab.prototype.showError = function(text) {
    this.hideSuccess();
    $(this.errorElement).text(text).show('normal');
}

Lab.prototype.hideError = function() {
    $(this.errorElement).hide();
}

Lab.prototype.showSuccess = function(text) {
    this.hideError();
    $(this.successElement).text(text).show('normal');
}

Lab.prototype.hideSuccess = function() {
    $(this.successElement).hide();
}

Lab.prototype.getSelectorText = function() {
    return $(this.selectorInputElement).val();
}

Lab.prototype.setExerciseText = function(text) {
    $(this.exerciseTextElement).html(text);
}

Lab.prototype.updateProgressElement = function() {

    var exerciseCount = this.getCurrentExercises().length;
    var pos = this.exercisePosition + 1;
    $(this.progressElement).html(pos + '/' + exerciseCount);
}

Lab.prototype.showSolution = function() {
    var solution = this.getCurrentExercise().solution;
    $(this.exerciseSolutionElement).text(solution).show('normal');
}

Lab.prototype.hideSolution = function() {
    $(this.exerciseSolutionElement).hide();
}

Lab.prototype.updateElements = function() {
    var exerciseText = this.getCurrentExercise().text;
    this.setExerciseText(exerciseText);
    this.hideSolution();
    this.hideError();
    this.hideSuccess();
    this.updateProgressElement();
    
   
    var solution = this.getCurrentExercise().solution;
    var solutionItems = $(this.verifySolutionElement).add(this.showSolutionElement);
    if(solution == null | solution == '') {
        $(solutionItems).hide();
    }
    else {
        $(solutionItems).show();
    }
    
}

Lab.prototype.goPrevious = function() {
   if(this.exercisePosition > 0) {
        this.exercisePosition--;
        this.updateElements();
    }
}

Lab.prototype.goNext = function() {
    var exerciseCount = this.getCurrentExercises().length;
    if(this.exercisePosition <  exerciseCount - 1 ) {
        this.exercisePosition++;
        this.updateElements();
    }
}


Lab.prototype.getSelectedLab = function() {
    return $('option:selected', this.exerciseSelect).index();
}

Lab.prototype.getCurrentDomUrl = function() {
    return this.data[this.getSelectedLab()].dom;
}

Lab.prototype.getCurrentExercises = function() {
    return this.data[this.getSelectedLab()].exercises;
}

Lab.prototype.getCurrentExercise = function() {
    return this.getCurrentExercises()[this.exercisePosition];
}

Lab.prototype.verifySolution = function() {
    var answer = this.getSelectorText();
    var solution = this.getCurrentExercise().solution;
    
    var answerElements;
    var solutionElements;
    try {
        answerElements = $(answer, this.sampleDataElement);
    }
    catch(err) {
        this.showError(
           'You entered an invalid jQuery expression'
        );
        return false;
    }
    try {
        solutionElements = $(solution, this.sampleDataElement);
    }
    catch(err) {
        this.showError(
            'Well this is embarassing. It seems that the solution ' +
            'is not working correctly.'
        );
        return false;
    }
    
    if(answerElements.length != solutionElements.length) {
        var text = 'The number of elements selected (' + answerElements.length 
            + ') does not match the number of elements selected by the solution (' +
            solutionElements.length + ')';
        this.showError(text);
        return false;
    }
    
    for(var i = 0; i < answerElements.length; i++) {
        if(answerElements[i] !== solutionElements[i]) {
            var text = 'The elements selected do not match elements selected by the solution';
            this.showError(text);
            return false;
        }
    }
    
    this.showSuccess('Success!');
    this.showSolution();
    return true;
}






