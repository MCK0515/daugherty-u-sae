import {Component} from "@angular/core";
import {User} from "../shared/user";
import {MatSnackBar} from "@angular/material";
import {Credentials} from '../shared/credentials';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["../../../shared/styles/home.scss"]
})
export class HomeComponent {
  private readonly snackBar: MatSnackBar;

  public loggedInUser: User;
  public credentials: Credentials = new Credentials();

  constructor(snackBar: MatSnackBar) {
    this.snackBar = snackBar;
  }

  public login(): void {
    // TODO: login user using authService to replace this existing logic
    if (this.credentials.username && this.credentials.password) {
      const user = new User();
      user.username = this.credentials.username;
      this.loggedInUser = user;
    } else {
      this.snackBar.open("Failed to login user!", undefined, {
        duration: 3000,
        verticalPosition: "top"
      });
    }
  }
}
