# DBS SA&E Angular Exercises

## Exercise 7

### Angular Concepts

* Using the angular-cli to generate a user routing module, user module, and user component
* Using angular routing to navigate through the application

### Details

Enhance login even further by taking the user to a new page that will be a placeholder for showing all users in the system upon successful login.
There is an example of routing in `header.component.ts` in the logout function that you can reference.
 
To create the new page, you will need a new directory under `src` called `user` and the following files:

- user.component.html (include a heading of "Users" so it's easy to tell that you've routed to the page correctly)
- user.component.ts
- user.module.ts
- user-routing.module.ts

TIP: You can use the following angular-cli commands to create the aforementioned files. They need to be run from the `Exercises` folder.

* `ng generate module ../user --project=exercise7 --routing=true`
* `ng generate component ../user --project=exercise7 --module=user --spec=false`

Do not forget to handle dependency injections where necessary.

You will also need to replace the `<app-home>` tag in the `app.component.html` with `<router-outlet>` to dynamically load routes. This requires the `HomeComponent` page to be adjusted to use a
`home-routing.module.ts`

### Web Resources

* https://angular.io/guide/ngmodules
* https://angular.io/cli/generate
* https://angular.io/guide/router

