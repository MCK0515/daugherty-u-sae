import {NgModule} from "@angular/core";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material";
import {RouterModule} from "@angular/router";
import {HeaderComponent} from "./header.component";
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    FlexLayoutModule,
    CommonModule,
    MatButtonModule,
    RouterModule
  ],
  exports: [HeaderComponent]
})
export class HeaderModule {
}
