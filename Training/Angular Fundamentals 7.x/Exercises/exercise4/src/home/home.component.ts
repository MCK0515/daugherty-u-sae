import {Component, OnDestroy, OnInit} from "@angular/core";
import {User} from "../shared/user";
import {MatSnackBar} from "@angular/material";
import {Credentials} from '../shared/credentials';
import {Subscription} from 'rxjs';
import {AuthService} from '../services/auth.service';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["../../../shared/styles/home.scss"]
})
export class HomeComponent implements OnDestroy, OnInit {
  private readonly snackBar: MatSnackBar;
  private readonly authService: AuthService;

  public loggedInUser: User;
  public credentials: Credentials = new Credentials();
  private userSubscription: Subscription; // TODO: Replace this subscription
  private loginSubscription: Subscription;

  constructor(snackBar: MatSnackBar, authService: AuthService) {
    this.snackBar = snackBar;
    this.authService = authService;
  }

  ngOnInit(): void {
    this.userSubscription = this.authService.getUser().subscribe((user: User) => this.loggedInUser = user);
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
    this.loginSubscription && this.loginSubscription.unsubscribe();
  }

  public login(): void {
    this.loginSubscription = this.authService.login(this.credentials).subscribe(null, (error: string) => {
      this.snackBar.open("Failed to login user!", undefined, {
        duration: 3000,
        verticalPosition: "top"
      });
    });
  }
}
