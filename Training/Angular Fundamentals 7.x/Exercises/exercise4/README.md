# DBS SA&E Angular Exercises

## Exercise 4

### Angular Concepts

* Use the angular async pipe as an alternative to subscriptions

### Details

Now that you've learned how to subscribe and unsubscribe to Observables, let's trim up the code a bit and use Angular's
built-in "async pipe" which will automatically subscribe to and unsubscribe from observables for you.

Adjust both the `HeaderComponent` and `HomeComponent` (code & template) to utilize the async pipe in order to get the value
of the logged in user after login. 

NOTE: The convention for an Observable variable is to the end the variable name with `$`. e.g. `loggedInUser$`
NOTE: You'll need to import the `CommonModule` from `@angular/common` and put it into the `imports` of your `HomeModule` to use the async pipe.

### Web Resources

* https://angular.io/api/common/AsyncPipe
* https://toddmotto.com/angular-ngif-async-pipe

