import { Component } from "@angular/core";
import { User } from "../shared/user";
import {Credentials} from '../shared/credentials';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["../../../shared/styles/home.scss"]
})
export class HomeComponent {
  public loggedInUser: User;
  // TODO: Bind username and password to inputs
  public credentials: Credentials = new Credentials();

  // TODO: Bind login to button
  public login(): void {
    // TODO: Validate inputs are defined
    // TODO: If valid, assign a new User object (with username) to loggedInUser
  }
}
