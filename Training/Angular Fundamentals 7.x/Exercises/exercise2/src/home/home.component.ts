import {Component} from "@angular/core";
import {User} from "../shared/user";
import {MatSnackBar} from "@angular/material";
import {Credentials} from '../shared/credentials';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["../../../shared/styles/home.scss"]
})
export class HomeComponent {
  public loggedInUser: User;
  public credentials: Credentials = new Credentials();

  public login(): void {
    if (this.credentials.username && this.credentials.password) {
      const user = new User();
      user.username = this.credentials.username;
      this.loggedInUser = user;
    } else {
      // TODO: replace console.log to snackBar.open
      console.log("Failed to login user!");
    }
  }
}
