# DBS SA&E Angular Exercises

## Exercise 0

### Angular Concepts

* Install dependencies
* Run application
* Explore application structure

### Details

You have been provided a basic (empty) website with header/footer and home page. When running the app, 
you should be able to open your browser to http://localhost:4200 and see this basic web.
