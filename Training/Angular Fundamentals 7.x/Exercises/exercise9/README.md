# DBS SA&E Angular Exercises

## Exercise 9

### Angular Concepts

* Loading data for a page component
* Use ng-template to render different content when data is not yet loaded
* Showing a list of items using ngFor

### Details

Display all users on the page utilizing an API call to `GET /api/users` from a new a function `getUsers` that you'll need create 
inside a new Service `UserService` which you'll need to inject into the `UserComponent`.

TIP: You can use the following angular-cli command to generate a `UserService`: 

* `ng generate service ../user/user --project=exercise9  --spec=false`

Use component the component's lifecycle hooks to fetch the data and in your template don't try to render the users until they have been loaded! 

i.e. Use `*ngIf` or `| async` with "else" referencing an ng-template that shows a loading icon, text, etc. 

Display the users (subscribe OR async pipe) in a table with the following attributes: `id`, `username`, `firstName`, `lastName`, `email`, `phone`, and `dob`. 
We'll use the `MatTableModule` but you do not need to.

### Web Resources

* https://coryrylan.com/blog/angular-async-data-binding-with-ng-if-and-ng-else
* https://angular.io/guide/structural-directives#the-ng-template
* https://blog.angular-university.io/angular-ng-template-ng-container-ngtemplateoutlet/
* https://angular.io/guide/structural-directives#inside-ngfor
* https://material.angular.io/components/table/examples
