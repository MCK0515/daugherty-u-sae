import {NgModule} from "@angular/core";
import {FlexLayoutModule} from "@angular/flex-layout";
import {UserComponent} from "./user.component";
import {UserRoutingModule} from "./user-routing.module";
import {MatCheckboxModule, MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatTableModule} from "@angular/material";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {CdkTableModule} from "@angular/cdk/table";
import {HttpClientModule} from "@angular/common/http";
import {UserService} from "./user.service";
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";
import {UserViewModule} from "./user-view/user-view.module";

@NgModule({
  declarations: [
    UserComponent
  ],
  imports: [
    FlexLayoutModule,
    CommonModule,
    FormsModule,
    CdkTableModule,
    MatTableModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    UserViewModule,
    UserRoutingModule
  ],
  exports: [
    UserComponent
  ],
  providers: [
    UserService
  ]
})
export class UserModule {
}
