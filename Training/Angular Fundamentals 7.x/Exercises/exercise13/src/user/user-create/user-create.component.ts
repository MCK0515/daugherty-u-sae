import {Component, OnDestroy} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../../shared/user";
import {UserService} from "../user.service";
import {NgForm} from "@angular/forms";
import {MatSnackBar} from "@angular/material";
import {Subscription} from 'rxjs';

@Component({
  templateUrl: "./user-create.component.html",
  styleUrls: ["../../../../shared/styles/user.scss"]
})
export class UserCreateComponent implements OnDestroy {
  private userService: UserService;
  private router: Router;
  private snackBar: MatSnackBar;
  private createUserSubscription: Subscription;

  public user: User = new User();

  constructor(userService: UserService, router: Router, snackBar: MatSnackBar) {
    this.userService = userService;
    this.router = router;
    this.snackBar = snackBar;
  }

  ngOnDestroy(): void {
    this.createUserSubscription && this.createUserSubscription.unsubscribe();
  }

  public createUser(form: NgForm): void {
    if (form.valid) {
      this.createUserSubscription = this.userService.createUser(this.user).subscribe(() => {
        this.router.navigate([`users`]);
      }, () => {
        this.snackBar.open('There was an error creating the user', undefined, {
          duration: 3000,
          verticalPosition: "top"
        });
      });
    }
  }
}
