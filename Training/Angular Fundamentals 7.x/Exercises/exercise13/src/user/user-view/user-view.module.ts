import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule, MatCardModule, MatSnackBarModule} from "@angular/material";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import {UserViewComponent} from "./user-view.component";

@NgModule({
  declarations: [
    UserViewComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ],
  exports: [
    UserViewComponent
  ]
})
export class UserViewModule {
}
