import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {User} from "../shared/user";
import {Result} from "../shared/result";
import {map, share} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {UserSearchParams} from './models/userSearchParams';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private http: HttpClient;

  constructor(httpClient: HttpClient) {
    this.http = httpClient;
  }

  public getUsers(userSearchParams?: UserSearchParams): Observable<User[]> {
    let params: HttpParams = new HttpParams();
    if (userSearchParams) {
      params = params.append("filterProp", userSearchParams.filterProp);
      params = !userSearchParams.filterText ? params : params.append("filterText", userSearchParams.filterText);
      params = params.append("sortAsc", userSearchParams.sortAsc ? "true" : "false");
    }
    return this.http.get<Result<User[]>>("/api/users", {params})
      .pipe(
        map((response: Result<User[]>) => response._data),
        share()
      );
  }

  public getUserById(id: number): Observable<User> {
    return this.http.get<Result<User>>(`/api/users/${id}`)
      .pipe(
        map((result: Result<User>) => result._data),
        share()
      );
  }

  public createUser(user: User): Observable<User> {
    return this.http.post<Result<User>>("api/users", user)
      .pipe(
        map((result: Result<User>) => result._data)
      );
  }
}
