# DBS SA&E Angular Exercises

## Exercise 8

### Angular Concepts

* Adding a hyperlink using angular routerLink

### Details

Add a hyperlink to navigate to the Users page with path `'/users'` in the header menu and conditionally show 
it only if a user has been authenticated using `*ngIf`.

### Web Resources

* https://angular.io/api/router/RouterLink


