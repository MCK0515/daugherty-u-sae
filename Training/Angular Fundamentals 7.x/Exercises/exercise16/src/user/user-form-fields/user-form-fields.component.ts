import { Component, Input } from "@angular/core";
import { User } from "../../shared/user";

@Component({
  selector: "app-user-form-fields",
  templateUrl: "./user-form-fields.component.html",
  styleUrls: ["../../../../shared/styles/user.scss"]
})
export class UserFormFieldsComponent {
  @Input()
  public user: User;
}