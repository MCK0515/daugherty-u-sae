import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule, MatCardModule, MatIconModule, MatSnackBarModule} from "@angular/material";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {RouterModule} from "@angular/router";
import {UserViewComponent} from "./user-view.component";
import {GenderPipe} from "../../pipes/gender.pipe";

@NgModule({
  declarations: [
    UserViewComponent,
    GenderPipe
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatIconModule,
    RouterModule
  ],
  exports: [
    UserViewComponent
  ]
})
export class UserViewModule {
}
