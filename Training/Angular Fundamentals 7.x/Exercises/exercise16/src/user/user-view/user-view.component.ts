import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material";
import {UserService} from "../user.service";
import {User} from "../../shared/user";
import {Observable, Subscription} from 'rxjs';

@Component({
  templateUrl: "./user-view.component.html",
  styleUrls: ["../../../../shared/styles/user.scss", "../../../../shared/styles/loading.scss"]
})
export class UserViewComponent implements OnInit, OnDestroy {
  private userService: UserService;
  private route: ActivatedRoute;
  private snackBar: MatSnackBar;
  private router: Router;
  private deleteSubscription: Subscription;

  public user$: Observable<User>;

  constructor(userService: UserService, route: ActivatedRoute, snackBar: MatSnackBar, router: Router) {
    this.userService = userService;
    this.route = route;
    this.snackBar = snackBar;
    this.router = router;
  }

  ngOnInit(): void {
    this.getUserById();
  }

  ngOnDestroy(): void {
    this.deleteSubscription && this.deleteSubscription.unsubscribe();
  }

  private getUserById(): void {
    try {
      const id: number = Number.parseInt(this.route.snapshot.paramMap.get("id"));
      this.user$ = this.userService.getUserById(id);
    } catch (error) {
      this.snackBar.open('Failed to load user', undefined, {
        duration: 3000,
        verticalPosition: "top"
      });
    }
  }

  public async deleteUser(user: User): Promise<void> {
    this.deleteSubscription = this.userService.deleteUser(user.id).subscribe(() => {
      this.router.navigate(["users"]);
    }, (error) => {
      this.snackBar.open('Failed to delete user', undefined, {
        duration: 3000,
        verticalPosition: "top"
      });
    });
  }
}
