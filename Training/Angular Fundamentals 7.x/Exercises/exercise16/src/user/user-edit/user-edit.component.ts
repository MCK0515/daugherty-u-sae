import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material";
import {NgForm} from "@angular/forms";
import {UserService} from "../user.service";
import {User} from "../../shared/user";
import {Observable, Subscription} from 'rxjs';

@Component({
  templateUrl: "./user-edit.component.html",
  styleUrls: ["../../../../shared/styles/user.scss", "../../../../shared/styles/loading.scss"]
})
export class UserEditComponent implements OnInit, OnDestroy {
  private userService: UserService;
  private router: Router;
  private snackBar: MatSnackBar;
  private route: ActivatedRoute;
  private editUserSubscription: Subscription;

  public user$: Observable<User>;

  constructor(userService: UserService, router: Router, snackBar: MatSnackBar, route: ActivatedRoute) {
    this.userService = userService;
    this.router = router;
    this.snackBar = snackBar;
    this.route = route;
  }

  ngOnInit(): void {
    const id: number = Number.parseInt(this.route.snapshot.paramMap.get("id"));
    this.user$ = this.userService.getUserById(id);
  }

  ngOnDestroy(): void {
    this.editUserSubscription && this.editUserSubscription.unsubscribe();
  }

  public updateUser(user: User, form: NgForm): void {
    if (form.valid) {
      this.editUserSubscription = this.userService.updateUser(user).subscribe(() => {
        this.router.navigate([`users`]);
      }, () => {
        this.snackBar.open('There was an error updating the user', undefined, {
          duration: 3000,
          verticalPosition: "top"
        });
      });
    }
  }
}
