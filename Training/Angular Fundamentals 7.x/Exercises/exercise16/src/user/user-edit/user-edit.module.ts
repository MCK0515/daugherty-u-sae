import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule, MatCardModule, MatFormFieldModule, MatProgressSpinnerModule, MatSnackBarModule} from "@angular/material";
import {CommonModule} from "@angular/common";
import {UserEditComponent} from "./user-edit.component";
import {UserFormFieldsModule} from "../user-form-fields/user-form-fields.module";

@NgModule({
  declarations: [
    UserEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSnackBarModule,
    UserFormFieldsModule,
    MatProgressSpinnerModule,
    RouterModule
  ],
  exports: [
    UserEditComponent
  ]
})
export class UserEditModule {
}
