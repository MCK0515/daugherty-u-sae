import { Component, OnInit } from "@angular/core";
import { User } from "../shared/user";
import {UserService} from './user.service';
import {Observable} from 'rxjs';
import {UserFilterOption} from './models/userFilterOption';
import {UserSearchParams} from './models/userSearchParams';
import {Router} from '@angular/router';

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["../../../shared/styles/user.scss", "../../../shared/styles/loading.scss"]
})
export class UserComponent implements OnInit {
  private readonly userService: UserService;
  private readonly router: Router;
  public users$: Observable<User[]>;
  public searchFor: string;
  public searchBy: string;
  public sortAsc: boolean;
  public userColumns: string[] = ["id", "username", "firstName", "lastName", "email", "phone", "dob"];
  public userFilters: UserFilterOption[] = [
    { name: "Id", value: "id" },
    { name: "First Name", value: "firstName" },
    { name: "Last Name", value: "lastName" },
    { name: "Username", value: "username" },
    { name: "Email", value: "email" }
  ];

  constructor(userService: UserService, router: Router) {
    this.userService = userService;
    this.router = router
  }

  ngOnInit(): void {
    this.searchBy = "id";
    this.sortAsc = true;
    this.getUsers();
  }

  public search(): void {
    const userSearchParams: UserSearchParams = new UserSearchParams();
    userSearchParams.filterProp = this.searchBy;
    userSearchParams.filterText = this.searchFor;
    userSearchParams.sortAsc = this.sortAsc;

    this.getUsers(userSearchParams);
  }

  public navigateToUser(user: User): void {
    this.router.navigate([`users/${user.id}`]);
  }

  private getUsers(searchParams?: UserSearchParams): void {
    this.users$ = this.userService.getUsers(searchParams);
  }
}
