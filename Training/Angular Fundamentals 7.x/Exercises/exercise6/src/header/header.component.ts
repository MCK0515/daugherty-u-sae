import {Component, OnInit} from "@angular/core";
import {User} from "../shared/user";
import {AuthService} from "../services/auth.service";
import {Observable} from 'rxjs';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["../../../shared/styles/header.scss"]
})
export class HeaderComponent implements OnInit {
  private readonly authService: AuthService;

  public user$: Observable<User>;

  constructor(authService: AuthService) {
    this.authService = authService;
  }

  ngOnInit(): void {
    this.user$ = this.authService.getUser();
  }

  // TODO: Handle logout
}
