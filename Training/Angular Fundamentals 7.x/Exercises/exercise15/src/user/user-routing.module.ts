import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "./user.component";
import {UserViewComponent} from './user-view/user-view.component';
import {UserCreateComponent} from './user-create/user-create.component';
import {UserEditComponent} from './user-edit/user-edit.component';

const routes: Routes = [
  {
    path: "users", component: UserComponent
  },
  {
    path: "users/create", component: UserCreateComponent
  },
  {
    path: "users/:id", component: UserViewComponent
  },
  {
    path: "users/:id/edit", component: UserEditComponent
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class UserRoutingModule {
}
