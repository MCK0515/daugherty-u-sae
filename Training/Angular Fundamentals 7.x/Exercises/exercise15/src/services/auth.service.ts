import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable, ReplaySubject, throwError} from "rxjs";
import {LocalStorageService} from "./localStorage.service";
import {User} from "../shared/user";
import {Result} from "../shared/result";
import {Credentials} from '../shared/credentials';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class AuthService {
  private readonly localStorageService: LocalStorageService;
  private readonly http: HttpClient;

  private userSubject = new ReplaySubject<User>();

  constructor(localStorageService: LocalStorageService, httpClient: HttpClient) {
    this.localStorageService = localStorageService;
    this.http = httpClient;
    this.init();
  }

  private init(): void {
    const user: User | null = this.getUserFromLocalStorage();

    if (user !== null) {
      this.userSubject.next(user);
    }
  }

  public login(credentials: Credentials): Observable<User> {
    return this.http.post<Result<User>>("/api/login", credentials)
      .pipe(
        map((result: Result<User>): User => {
          const loggedInUser: User = result._data;
          this.userSubject.next(loggedInUser);
          this.localStorageService.setItem("user", JSON.stringify(loggedInUser));
          return loggedInUser;
        }),
        catchError((e) => throwError(e.error._errors[0]))
      );
  }

  public logout(): Observable<void> {
    return this.http.post<Result<void>>("/api/logout", {})
      .pipe(
        map(() => {
          this.localStorageService.removeItem("user");
          this.userSubject.next(undefined);
        }),
        catchError(() => throwError("Failed to logout user"))
      );
  }

  public getUser(): Observable<User> {
    return this.userSubject.asObservable();
  }

  private getUserFromLocalStorage(): User | null {
    const userJsonStr: string | null = this.localStorageService.getItem("user");

    if (userJsonStr) {
      return JSON.parse(userJsonStr);
    }

    return null;
  }
}
