# DBS SA&E Angular Exercises

## Exercise 15

### Angular Concepts

* Use the angular pipe to transform data for displaying

### Details

Enhance `user-view.component.html` to transform `gender` property values:

* `M` to `Male `
* `F` to `Female`

You will need to create angular pipe `GenderPipe` in a new directory `pipes`.

### Web Resources

* https://angular.io/api/core/Pipe
* https://angular.io/guide/pipes
