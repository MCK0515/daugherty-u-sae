import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from 'rxjs/operators';
import { Result } from "../shared/result";
import { User } from "../shared/user";
import { UserSearchParams } from "./models/userSearchParams";

@Injectable()
export class UserService {
  private http: HttpClient;

  constructor(httpClient: HttpClient) {
    this.http = httpClient;
  }

  public async getUsers(userSearchParams?: UserSearchParams): Promise<User[]> {
    let params: HttpParams = new HttpParams();
    if (userSearchParams) {
      params = params.append("filterProp", userSearchParams.filterProp);
      params = !userSearchParams.filterText ? params : params.append("filterText", userSearchParams.filterText);
      params = params.append("sortAsc", userSearchParams.sortAsc ? "true" : "false");
    }
    return this.http.get<Result<User[]>>("/api/users", { params })
      .pipe(map((response: Result<User[]>) => response._data))
      .toPromise()
      .catch(() => Promise.reject("Failed to get users"));
  }

  public async getUserById(id: number): Promise<User> {
    return this.http.get<Result<User>>(`/api/users/${id}`)
      .pipe(map((result: Result<User>) => result._data))
      .toPromise()
      .catch(() => Promise.reject("Failed to get user"));
  }

  public async createUser(user: User): Promise<User> {
    return this.http.post<Result<User>>("api/users", user)
      .pipe(map((result: Result<User>) => result._data))
      .toPromise()
      .catch(() => Promise.reject("Failed to create user"));
  }

  public async updateUser(user: User): Promise<User> {
    return this.http.put<Result<User>>(`api/users/${user.id}`, user)
      .pipe(map((result: Result<User>) => result._data))
      .toPromise()
      .catch(() => Promise.reject("Failed to update user"));
  }

  public async deleteUser(id: number): Promise<void> {
    return this.http.delete<void>(`api/users/${id}`).toPromise();
  }
}