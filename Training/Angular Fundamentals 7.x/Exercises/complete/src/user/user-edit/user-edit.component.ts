import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { NgForm } from "@angular/forms";

import { UserService } from "../user.service";
import { User } from "../../shared/user";

@Component({
  templateUrl: "./user-edit.component.html",
  styleUrls: ["../../../../shared/styles/user.scss"]
})
export class UserEditComponent implements OnInit {
  private userService: UserService;
  private router: Router;
  private snackBar: MatSnackBar;
  private route: ActivatedRoute;

  public user: User = new User();

  constructor(userService: UserService, router: Router, snackBar: MatSnackBar, route: ActivatedRoute) {
    this.userService = userService;
    this.router = router;
    this.snackBar = snackBar;
    this.route = route;
  }

  ngOnInit(): void {
    this.route.data.subscribe(({ user }) => {
      this.user = user;
    });
  }

  public async updateUser(form: NgForm): Promise<void> {
    if (form.valid) {
      try {
        await this.userService.updateUser(this.user);
        this.router.navigate([`users`]);
      } catch (error) {
        this.snackBar.open(<string>error, undefined, {
          duration: 3000,
          verticalPosition: "top"
        });
      }
    }
  }
}