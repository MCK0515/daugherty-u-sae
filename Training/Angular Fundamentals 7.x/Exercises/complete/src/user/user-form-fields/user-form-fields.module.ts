import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatCardModule, MatInputModule, MatButtonModule, MatFormFieldModule, MatRadioModule } from "@angular/material";
import { CommonModule } from "@angular/common";
import { UserFormFieldsComponent } from "./user-form-fields.component";

@NgModule({
  declarations: [
    UserFormFieldsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatFormFieldModule,
    MatRadioModule,
  ],
  exports: [
    UserFormFieldsComponent
  ]
})
export class UserFormFieldsModule { }