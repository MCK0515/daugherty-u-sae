import {NgModule} from "@angular/core";
import {FlexLayoutModule} from "@angular/flex-layout";

import {UserComponent} from "./user.component";
import {UserRoutingModule} from "./user-routing.module";
import {MatTableModule} from "@angular/material";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {CdkTableModule} from "@angular/cdk/table";
import {HttpClientModule} from "@angular/common/http";
import {UserService} from "./user.service";
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    UserComponent
  ],
  imports: [
    FlexLayoutModule,
    CommonModule,
    CdkTableModule,
    MatTableModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    UserRoutingModule
  ],
  exports: [
    UserComponent
  ],
  providers: [
    UserService
  ]
})
export class UserModule {
}
