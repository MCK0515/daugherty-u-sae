import { Component, OnInit } from "@angular/core";
import { User } from "../shared/user";
import {UserService} from './user.service';
import {Observable} from 'rxjs';

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["../../../shared/styles/user.scss", "../../../shared/styles/loading.scss"]
})
export class UserComponent implements OnInit {
  private readonly userService: UserService;
  public users$: Observable<User[]>;
  public userColumns: string[] = ["id", "username", "firstName", "lastName", "email", "phone", "dob"];

  constructor(userService: UserService) {
    this.userService = userService;
  }

  ngOnInit(): void {
    this.users$ = this.userService.getUsers();
  }
}
