import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {User} from "../shared/user";
import {Result} from "../shared/result";
import {map, share} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private http: HttpClient;

  constructor(httpClient: HttpClient) {
    this.http = httpClient;
  }

  public getUsers(): Observable<User[]> {
    return this.http.get<Result<User[]>>("/api/users")
      .pipe(
        map((response: Result<User[]>) => response._data),
        share()
      );
  }
}
