/**
 * Function that finds and returns the biggest positive divisor of the given positive integer
 */
function findBiggestDivisor (positiveInteger) {
    console.log("Finding the biggest divisor of: " + positiveInteger);
    // Your code goes here
}

// Test it with different values
console.log(findBiggestDivisor(7));

