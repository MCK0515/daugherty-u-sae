import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { User } from "../../shared/user";
import { UserService } from "../user.service";
import { NgForm } from "@angular/forms";
import { MatSnackBar } from "@angular/material";

@Component({
  templateUrl: "./user-create.component.html",
  styleUrls: ["../../../../shared/styles/user.scss"]
})
export class UserCreateComponent {
  private userService: UserService;
  private router: Router;
  private snackBar: MatSnackBar;

  public user: User = new User();

  constructor(userService: UserService, router: Router, snackBar: MatSnackBar) {
    this.userService = userService;
    this.router = router;
    this.snackBar = snackBar;
  }

  public async createUser(form: NgForm): Promise<void> {
    if (form.valid) {
      try {
        await this.userService.createUser(this.user);
        this.router.navigate([`users`]);
      } catch (error) {
        this.snackBar.open(<string>error, undefined, {
          duration: 3000,
          verticalPosition: "top"
        });
      }
    }
  }
}