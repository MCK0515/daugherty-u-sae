# DBS SA&E Angular Exercises

## Exercise 12

### Angular Concepts

* passing data to a Component using @Input
* HTTP PUT

### Details

Add the ability for editing a user. You will need to create a new module `UserEditModule` and wire it up to a new component, `UserEditComponent` and template, `user-edit.component.html`. Because creating and editing a user modify the same user properties, make use of the `UserFormComponent` using `@Input` property to share the form fields between both create and edit user forms.

You will need to add a way to get to the edit user page from the View User page.

Enhance `UserService` to handle updating a user. You should use the RESTful API `PUT /api/users/{id}` with the user as the body of the request. Upon successful update of the user, redirect to the Users page with the updated user list.

### Web Resources

* https://angular.io/guide/component-interaction
* https://angular.io/api/http/Http#put