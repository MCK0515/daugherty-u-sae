# DBS SA&E Angular Exercises

## Exercise 8

### Angular Concepts

* Using angular Resolve
* Injection of resolved variables
* Showing a list of items using ngFor

### Details

Display all users on the page utilizing an API call to `GET /api/users` from a new `userService.getUsers` method from the `UserResolver` class located at `user/user.resolve.ts` and inject the resolved variables `users` via `ActivatedRoute` (which will need to be injected) into the `UserComponent`. Display the users in a table
with the following attributes: `id`, `username`, `firstName`, `lastName`, `email`, `phone`, and `dob`. We'll use the `MatTableModule` but you do not need to.

### Web Resources

* https://angular.io/api/router/Resolve
* https://angular.io/api/router/ActivatedRoute
* https://angular.io/guide/router#fetch-data-before-navigating
* https://angular.io/guide/structural-directives#inside-ngfor
* https://material.angular.io/components/table/examples
