# DBS SA&E Angular Exercises

## Exercise 13

### Angular Concepts

* HTTP DELETE

### Details

Add the ability for deleting a user when viewing a user.

Enhance `UserService` to handle deleting a user. You should use the RESTful API `DELETE /api/users/{id}`. Upon successful
delete of the user, reload the Users page with the updated user list.


### Web Resources

* https://angular.io/api/http/Http#delete

