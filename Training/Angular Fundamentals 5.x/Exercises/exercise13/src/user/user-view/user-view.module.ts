import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatCardModule, MatButtonModule, MatSnackBarModule, MatIconModule } from "@angular/material";
import { RouterModule } from "@angular/router";

import { UserViewComponent } from "./user-view.component";

@NgModule({
  declarations: [
    UserViewComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    MatIconModule,
    RouterModule
  ],
  exports: [
    UserViewComponent
  ]
})
export class UserViewModule { }