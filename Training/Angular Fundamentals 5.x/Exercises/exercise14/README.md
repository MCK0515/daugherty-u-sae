# DBS SA&E Angular Exercises

## Exercise 14

### Angular Concepts

* Pipe

### Details

Add the ability for deleting a user on the Users page.

Enhance `user-view.component.html` to transform `gender` property values:

* `M` to `Male `
* `F` to `Female`

You will need to create angular pipe `GenderPipe` in a new directory `pipes`.


### Web Resources

* https://angular.io/api/core/Pipe
* https://angular.io/guide/pipes