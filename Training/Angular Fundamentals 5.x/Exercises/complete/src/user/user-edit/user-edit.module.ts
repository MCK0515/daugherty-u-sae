import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatCardModule, MatInputModule, MatButtonModule, MatFormFieldModule, MatRadioModule, MatSnackBarModule } from "@angular/material";
import { CommonModule } from "@angular/common";

import { UserEditComponent } from "./user-edit.component";
import { UserEditResolver } from "./user-edit.resolve";
import { UserFormFieldsModule } from "../user-form-fields/user-form-fields.module";

@NgModule({
  declarations: [
    UserEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSnackBarModule,
    UserFormFieldsModule,
    RouterModule
  ],
  exports: [
    UserEditComponent
  ]
})
export class UserEditModule { }