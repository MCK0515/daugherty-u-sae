export class User {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  dob: Date;
  active: boolean;
  password: string;
}