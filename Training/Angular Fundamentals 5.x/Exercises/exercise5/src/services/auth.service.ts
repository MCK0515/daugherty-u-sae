import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/map";

import { LocalStorageService } from "./localStorage.service";
import { User } from "../shared/user";
import { Result } from "../shared/result";

@Injectable()
export class AuthService {
  private readonly localStorageService: LocalStorageService;
  private readonly http: HttpClient;

  private userSubject = new ReplaySubject<User>();

  constructor(localStorageService: LocalStorageService, httpClient: HttpClient) {
    this.localStorageService = localStorageService;
    this.http = httpClient;
    this.init();
  }

  private init(): void {
    const userJsonStr: string | null = this.localStorageService.getItem("user");

    if (userJsonStr !== null) {
      const user: User = JSON.parse(userJsonStr);
      this.userSubject.next(user);
    }
  }

  public async login(user: User): Promise<void> {
    if (user.username && user.password) {
      try {
        const loggedInUser: User = await this.http.post<Result<User>>("/api/login", user)
          .map((result: Result<User>): User => result._data)
          .toPromise();
        this.userSubject.next(loggedInUser);
        this.localStorageService.setItem("user", JSON.stringify(loggedInUser));
      } catch (e) {
        return Promise.reject(e.error._errors[0]);
      }
    } else {
      return Promise.reject("Invalid username or password.");
    }
  }
  public getUser(): Observable<User> {
    return this.userSubject.asObservable();
  }
}