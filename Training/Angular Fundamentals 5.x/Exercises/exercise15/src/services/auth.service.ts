import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/map";

import { LocalStorageService } from "./localStorage.service";
import { User } from "../shared/user";
import { Result } from "../shared/result";
import { Router } from "@angular/router";

@Injectable()
export class AuthService {
  private userSubject = new ReplaySubject<User>();

  private localStorageService: LocalStorageService;
  private http: HttpClient;
  private router: Router;

  constructor(localStorageService: LocalStorageService, httpClient: HttpClient, router: Router) {
    this.localStorageService = localStorageService;
    this.http = httpClient;
    this.router = router;
    this.init();
  }

  private init(): void {
    const user: User | null = this.getUserFromLocalStorage();

    if (user !== null) {
      this.userSubject.next(user);
    }
  }

  public async login(user: User): Promise<void> {
    if (user.username && user.password) {
      try {
        const loggedInUser: User = await this.http.post<Result<User>>("/api/login", user)
          .map((result: Result<User>): User => result._data)
          .toPromise();
        this.userSubject.next(loggedInUser);
        this.localStorageService.setItem("user", JSON.stringify(loggedInUser));
      } catch (e) {
        return Promise.reject(e.error._errors[0]);
      }
    } else {
      return Promise.reject("Invalid username or password.");
    }
  }

  public async logout(): Promise<void> {
    try {
      await this.http.post<Result<void>>("/api/logout", {}).toPromise();
      this.localStorageService.removeItem("user");
      this.userSubject.next(undefined);
      this.router.navigate([""]);
    } catch (error) {
      return Promise.reject("Failed to logout user");
    }
  }

  public getUser(): Observable<User> {
    return this.userSubject.asObservable();
  }

  private getUserFromLocalStorage(): User | null {
    const userJsonStr: string | null = this.localStorageService.getItem("user");

    if (userJsonStr) {
      return JSON.parse(userJsonStr);
    }

    return null;
  }
}