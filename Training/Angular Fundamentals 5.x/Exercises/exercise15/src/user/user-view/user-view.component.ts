import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";

import { UserService } from "../user.service";
import { User } from "../../shared/user";

@Component({
  templateUrl: "./user-view.component.html",
  styleUrls: ["../../../../shared/styles/user.scss"]
})
export class UserViewComponent implements OnInit {
  private userService: UserService;
  private route: ActivatedRoute;
  private snackBar: MatSnackBar;
  private router: Router;

  public user: User;

  constructor(userService: UserService, route: ActivatedRoute, snackBar: MatSnackBar, router: Router) {
    this.userService = userService;
    this.route = route;
    this.snackBar = snackBar;
    this.router = router;
  }

  ngOnInit(): void {
    this.getUserById();
  }

  private async getUserById(): Promise<void> {
    try {
      const id: number = Number.parseInt(this.route.snapshot.paramMap.get("id"));
      this.user = await this.userService.getUserById(id);
    } catch (error) {
      this.snackBar.open(<string>error, undefined, {
        duration: 3000,
        verticalPosition: "top"
      });
    }
  }

  public async deleteUser(): Promise<void> {
    await this.userService.deleteUser(this.user.id);
    this.router.navigate(["users"]);
  }
}