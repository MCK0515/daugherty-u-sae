(function () {
  // ScrollSpy
  $('a.page-scroll').click(function() {
    var target = $(this.hash);
    $('html,body').animate({
      scrollTop: target.offset().top - 40
    }, 900);
    return false;
  });

  // affix navbar to top of page
  $('#nav').affix({
    offset: {
      top: $('header').height()
    }
  });

  // Fixes navbar issue when affix
  $('#nav').on('affix.bs.affix', function () {
    var navHeight = $('.navbar').outerHeight(true);
    $('#nav + #about').css('margin-top', navHeight);
  });
  $('#nav').on('affix-top.bs.affix', function () {
    $('#nav + #about').css('margin-top', 0);
  });
}());
