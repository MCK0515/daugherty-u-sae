# Assessment V2

This folder holds the re-imagined assessment process for the DU SA&E class of Q1 2018 and forward.

## Using the Assessment

There are several exercises in the _Questions_ folder, along with a framework and tests for checking candidate's responses.

There are more questions than the candidates will reasonably finish, so select a subset of them and compile them into an Assessment Guide which will be given to the candidates at assessment time. (See the past assessments in the _Assessments_ folder for a guide to creating new ones.)

The tests are run via `npm`. Before running the tests, dependencies must be fetched with the command `npm install` either in an IDE or from the command line in the `Tests/` directory.

In the file `Tests/package.json`, there are some changes to be made to suit your assessment. The key `useTestAnswers` when set to `true` will cause the test suite to use the example answers (to validate the correctness of the tests); when set to `false`, it can test a candidate's solutions. Additionally, the boolean values of the named tests determine if those test cases will be executed. For example, if the assessment does not use the exercise `AnalyzePrices`, that test should be set to `false`.

To test an candidate's solution, paste their code into the header section of `Tests/src/answers.js` and then run `npm test`.

## Adding to the Assessment

### Adding New Questions

Adding a new question is as simple as starting from the template, naming the question something unique, and adding it to the repository. To make sure it is easy to grade, see the next section to add matching tests.

### Adding New Tests
The test framework is pretty flexible, but to add support for a new question, there are a couple changes to be made:

#### Add JEST flag to turn test off/on

1. In the package.json, there is a config section for JEST (jest -> globals -> tests). Add a flag here that matches the question file name.

#### Add an answer to the answer key

1. In the src/answers folder, add a JS file that matches the question file name and exports the function that answers the question
2. In the src/answers.js file, add an import/export block with the other answer keys near the bottom

#### Add handling for testing a candidate's answer
1. In the src/answers.js file, add an optional import/export block to protect against a candidate not providing a function with that name

#### Add the test cases
1. In the test/ folder, add a \*.tests.js file that matches the answer key file name.
2. Import answers, set up the optional test declaration (see other tests for an example) using the flag name from the package.json config
3. Write test cases that cover the examples and other interesting edge cases
