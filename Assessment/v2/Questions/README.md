# Questions

| Question | Summary | Topics |
| -------- | ------- | ------ |
| AnagramTester | Validate if two strings are anagrams | String Processing, Arrays |
| AnalyzePrices | Find global max difference between values | Array Indexing, Object Creation |
| CarClass | Create constructor for 'Car' class | JS OOP, 'this', prototype |
| FizzBuzz | Variable loop that outputs text on modulus matches | Loops, Modulus |
| PrintObjectProperties | Print out key/value properties for an object | Object Manipulation, Loops |
| PropertyPathEvaluation | Print out nested value in an object based on provided path | Object Manipulation, Edge Cases |
| PythagoreanTheorem | Implement the Pythagorean theorem | |
| StringCapitalization | Capitalize the first character in each word in a string| String Processing |
| Summation | Sum all whole numbers between two numbers | Looping, Math |
| SumNestedArray | Recursively sum all numbers in an array of arrays | Recursion, Arrays |
| WordCount | Count all 'words' in a string | String Processing |

# Hard Questions
| Question | Summary | Topics |
| -------- | ------- | ------ |
| BowlingScores | Parse bowling score string & score it | String Processing, Loops, Array Indexing |
| MemoizationWrapper | Higher Order Function Generator for Memoization | Function Wrappers, Caching |
