let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.CarClass) {
  optionallyTest = describe;
}

optionallyTest("Exercise: Car Class", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it("has a 'Car' function", () => {
        expect(Answers.Car).toBeDefined();
    });

    it("Car has initial speed of 0", () => {
        const car = new Answers.Car();
        expect(car.getSpeed()).toBe(0);
    });

    it("Car function can be used to create an object with the right prototype", () => {
       const car = new Answers.Car();
       expect(car).toBeDefined();
       expect(car.getSpeed).toBeDefined();
       expect(car.setSpeed).toBeDefined();
       expect(car.stop).toBeDefined();
       expect(typeof car.getSpeed).toBe("function");
       expect(typeof car.setSpeed).toBe("function");
       expect(typeof car.stop).toBe("function");
    });

    it("Works for the example", () => {
        const car = new Answers.Car();
        expect(car.getSpeed()).toBe(0);
        car.setSpeed(10);
        expect(car.getSpeed()).toBe(10);
        car.stop();
        expect(car.getSpeed()).toBe(0);
    });

    it("Works if you call things in a different order than the example", () => {
        const car = new Answers.Car();
        expect(car.getSpeed()).toBe(0);
        car.stop();
        expect(car.getSpeed()).toBe(0);
        car.setSpeed(10);
        expect(car.getSpeed()).toBe(10);
    });

    it("handles the negative setSpeed edge case", () => {
        const car = new Answers.Car();
        expect(car.getSpeed()).toBe(0);
        car.setSpeed(10);
        expect(car.getSpeed()).toBe(10);
        car.setSpeed(-1);
        expect(car.getSpeed()).toBe(10);
        car.setSpeed(100);
        expect(car.getSpeed()).toBe(100);
    });

});
