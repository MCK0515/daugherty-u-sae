let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.AnalyzePrices) {
    optionallyTest = describe;
}

optionallyTest("Exercise: Analyze Prices", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it ("has an 'analyzePrices' function", () => {
        expect(Answers.analyzePrices).toBeDefined();
    });

    function testAnalyzePrices(array, buyIndex, sellIndex, name = undefined) {
        it(`${name? name + ": " : ""}[${array}] should return buyIndex ${buyIndex} and sellIndex ${sellIndex}`, () => {
            let result = Answers.analyzePrices(array);
            expect(result['buyIndex']).toBe(buyIndex);
            expect(result['sellIndex']).toBe(sellIndex);
        });
    }

    testAnalyzePrices([1,2,3,4,5,6,7,8,9,0], 0, 8, "Example 1");
    testAnalyzePrices([8,4,3,1,5,5,2,8,2,3], 3, 7, "Example 2");
    testAnalyzePrices([5,2,5,4,5,6,3,8,1,8], 8, 9, "Example 3");
    testAnalyzePrices([1,1,1,1,1], null, null, "Example 4: Flat Prices");

    testAnalyzePrices([5,4,3,2,1], null, null, "Monotonic Decreasing");
});
