let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.BowlingScores) {
    optionallyTest = describe;
}

optionallyTest("Exercise: Bowling Scores", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it ("has a 'calculateBowlingScore' function", () => {
        expect(Answers.calculateBowlingScore).toBeDefined();
    });

    function testCalculateBowlingScore(game, score, name) {
        it(`${name? name + ": " : ""}${game} should return a score of ${score}`, () => {
            let result = Answers.calculateBowlingScore(game);
            expect(result).toBe(score);
        });
    }

    testCalculateBowlingScore('XXXXXXXXXXXX', 300, "Example 1");
    testCalculateBowlingScore('1-1-1-1-1-1-1-1-1-1-', 10, "Example 2");
    testCalculateBowlingScore('1/1/1/1/1/1/1/1/1/1/1', 110, "Example 3");

    testCalculateBowlingScore('X-2712-X237/1-9-3/X', 85, "Random Game");
});
