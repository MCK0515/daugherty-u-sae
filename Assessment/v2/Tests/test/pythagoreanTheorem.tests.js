let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.PythagoreanTheorem) {
  optionallyTest = describe;
}

optionallyTest("Exercise: Pythagorean Theorem", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it("has a 'getHypotenuseLength' function", () => {
        expect(Answers.getHypotenuseLength).toBeDefined();
    });

    it("works for the example: (3, 4) => 5", () => {
        expect(Answers.getHypotenuseLength(3, 4)).toBe(5);
    });

    it("handles the a = 0 edge case", () => {
        expect(Answers.getHypotenuseLength(0, 4)).toBe(0);
    });

    it("handles the a < 0 edge case", () => {
        expect(Answers.getHypotenuseLength(-1, 4)).toBe(0);
    });

    it("handles the b = 0 edge case", () => {
        expect(Answers.getHypotenuseLength(3, 0)).toBe(0);
    });

    it("handles the b < 0 edge case", () => {
        expect(Answers.getHypotenuseLength(3, -1)).toBe(0);
    });

    it("works for (5, 5) => ~7.0710678", () => {
        expect(Answers.getHypotenuseLength(5, 5)).toBeCloseTo(7.0710678118654755);
    });


});
