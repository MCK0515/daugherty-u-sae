let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.AnagramTester) {
  optionallyTest = describe;
}


optionallyTest("Exercise: Anagram Tester", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it ("has an 'areTheseAnagrams' function", () => {
        expect(Answers.areTheseAnagrams).toBeDefined();
    });

    function testAreTheseAnagrams(string1, string2, answer, name = undefined) {
        it(`${name? name + ": " : ""}(${string1}, ${string2}) should return ${answer}`, () => {
            let result = Answers.areTheseAnagrams(string1, string2);
            expect(result).toBe(answer);
        });
    }

    testAreTheseAnagrams("abc", "bca", true, "Example 1");
    testAreTheseAnagrams("abc", "cde", false, "Example 2");

    testAreTheseAnagrams("god", "dog", true, "Misunderstood and only did palindromes?");

    testAreTheseAnagrams("abc", "aabbcc", false, "Edge Case: Different Length");
    testAreTheseAnagrams("abc", "abc", true, "Edge Case: Same String");
    testAreTheseAnagrams("aac", "acc", false, "Edge Case: Same Letters, Different Counts");
});
