let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.SumNestedArray) {
  optionallyTest = describe;
}

optionallyTest("Exercise: Sum Nested Arrays", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it ("has a 'sumNested' function", () => {
        expect(Answers.sumNested).toBeDefined();
    });

    function testSumNested(array, answer, name = undefined) {
        it(`${name? name + ": " : ""}[${array}] should return ${answer}`, () => {
            let result = Answers.sumNested(array);
            expect(result).toBe(answer);
        });
    }

    testSumNested([1, 1, 1, [3, 4, [8]], [5]], 23, "Example 1");

    testSumNested([], 0, "Edge Case: Empty Array");
    testSumNested([1, 2, 3, [], [5, 6, []], [[[]]]], 17, "Edge Case: Nested Empty Arrays");
});
