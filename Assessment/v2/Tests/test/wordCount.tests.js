let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.WordCount) {
  optionallyTest = describe;
}

optionallyTest("Exercise: Word Count", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it("has a 'wordCount' function", () => {
        expect(Answers.wordCount).toBeDefined();
    });

    function testWordCount(sentence, answer, name = undefined) {
        it(`${name? name + ": " : ""}"${sentence}" should return ${answer}`, () => {
            let result = Answers.wordCount(sentence);
            expect(result).toBe(answer);
        });
    }

    testWordCount("This is a short sentence!", 5, "Example 1");
    testWordCount("ThisIsA!$ReallyLongWord", 1, "Example 2");
    testWordCount("          ", 0, "Example 3");

    testWordCount("This   test   has   lots -of-   spaces.", 6, "Edge Case: Multiple Spaces Between Words");
    testWordCount("works", 1, "Edge Case: One Word");
    testWordCount("  two words", 2, "Edge Case: Leading Whitespace");
    testWordCount("three -new- words   ", 3, "Edge Case: Trailing Whitespace");
});
