let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.PrintObjectProperties) {
  optionallyTest = describe;
}

optionallyTest("Exercise: Print Object Properties", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    const oldConsoleLogFunction = console.log;
    let outputStrings = [];
    function captureOutput(consoleString) {
        outputStrings.push(consoleString);
    }

    afterEach(() => {
        outputStrings = [];
    });

    it("has a 'printKeyValues' function", () => {
        expect(Answers.printKeyValues).toBeDefined();
    });

    it("works for the example {key1: 'value1', key2: 'value2', 'key three': 'value3'}", () => {
        const object = {
            key1: "value1",
            key2: "value2",
            "key three": "value3"
        };
        console.log = captureOutput;
        Answers.printKeyValues(object);
        console.log = oldConsoleLogFunction;
        expect(outputStrings.length).toBe(3);
        expect(outputStrings[0]).toBe("key1: value1");
        expect(outputStrings[1]).toBe("key2: value2");
        expect(outputStrings[2]).toBe("key three: value3");
    });

    it("works for some other object {a: 'b', c: 'd'}", () => {
        const object = {
            a: "b",
            c: "d"
        };
        console.log = captureOutput;
        Answers.printKeyValues(object);
        console.log = oldConsoleLogFunction;
        expect(outputStrings.length).toBe(2);
        expect(outputStrings[0]).toBe("a: b");
        expect(outputStrings[1]).toBe("c: d");
    });

    it("works for the empty object {}", () => {
        console.log = captureOutput;
        Answers.printKeyValues({});
        console.log = oldConsoleLogFunction;
        expect(outputStrings.length).toBe(0);
    });

});
