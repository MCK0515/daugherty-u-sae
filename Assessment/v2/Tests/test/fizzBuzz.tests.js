let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.FizzBuzz) {
  optionallyTest = describe;
}

optionallyTest("Exercise: Fizz Buzz", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it("has a 'fizzBuzz' function", () => {
        expect(Answers.fizzBuzz).toBeDefined();
    });

    function testFizzBuzz(counter, answer, name = undefined) {
        it(`${name? name + ": " : ""}${counter} should return ${answer}`, () => {
            let result = Answers.fizzBuzz(counter);
            expect(result).toBe(answer);
        });
    }

    testFizzBuzz(0, "", "Example 1: 0");
    const exampleOutput = "1, 2, 3fizz, 4, 5buzz, 6fizz, 7, 8, 9fizz, 10buzz, 11, 12fizz, 13, 14, 15fizzbuzz";
    testFizzBuzz(15, exampleOutput, "Example 3");

    testFizzBuzz(-1, "", "Edge Case: Negative Index");
    const output_16 = "1, 2, 3fizz, 4, 5buzz, 6fizz, 7, 8, 9fizz, 10buzz, 11, 12fizz, 13, 14, 15fizzbuzz, 16";
    testFizzBuzz(16, output_16, "Test Case: 16");
    const output_2 = "1, 2";
    testFizzBuzz(2, output_2, "Test Case: 2");
});
