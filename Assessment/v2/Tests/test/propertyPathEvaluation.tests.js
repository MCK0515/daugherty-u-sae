let Answers = require("../src/answers.js");

let optionallyTest = describe.skip;
if (tests.PropertyPathEvaluation) {
    optionallyTest = describe;
}

optionallyTest("Exercise: Property Path Evaluation", () => {

    beforeAll(() => {
        if (useTestAnswers) {
            console.log("*****************************************************\n" +
                "*****************************************************\n" +
                " Using Answer Key Answers to Validate Tests\n" +
                "*****************************************************\n" +
                "*****************************************************");
        }
    });

    it("has a 'propertyValueAt' function", () => {
        expect(Answers.propertyValueAt).toBeDefined();
    });

    function testPropertyValueAt(obj, array, property, name) {
        it(`${name? name + ": " : ""}[${array}] should return ${property}`, function() {
            let result = Answers.propertyValueAt(obj, array);
            expect(result).toEqual(property);
        });
    }

    const object = {
        a: 1,
        b: {
            c: 2,
            d: 3
        },
        e: {
          f: {
              g: 1
          }
        }
    };
    testPropertyValueAt(object, ['a'], object.a, "Example 1");
    testPropertyValueAt(object, ['b'], object.b, "Example 2");
    testPropertyValueAt(object, ['b', 'd'], object.b.d, "Example 3");
    testPropertyValueAt(object, ['d'], undefined, "Example 4");
    testPropertyValueAt(object, ['q'], undefined, "Example 5");

    testPropertyValueAt(object, ['e', 'f', 'g'], object.e.f.g, "Edge Case: Nested Found");

    testPropertyValueAt(object, ['q', 'r', 's'], undefined, "BONUS - Edge Case: Nested Not Found - Check for existence before recursing");

});
