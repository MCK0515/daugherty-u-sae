///////////////////////////////
// PASTE CODE BELOW HERE
///////////////////////////////



///////////////////////////////
// DO NOT EDIT BELOW THIS LINE
///////////////////////////////

if (useTestAnswers) {

    let analyzePrices = require("./answers/analyzePrices");
    let areTheseAnagrams = require("./answers/anagramTester");
    let calculateBowlingScore = require("./answers/bowlingScores");
    let Car = require("./answers/carClass");
    let fizzBuzz = require("./answers/fizzBuzz");
    let printKeyValues = require("./answers/printObjectProperties");
    let propertyValueAt = require("./answers/propertyPathEvaluation");
    let getHypotenuseLength = require("./answers/pythagoreanTheorem");
    let capitalize = require("./answers/stringCapitalization");
    let summation = require("./answers/summation");
    let sumNested = require("./answers/sumNestedArray");
    let wordCount = require("./answers/wordCount");

    module.exports = {
        analyzePrices,
        areTheseAnagrams,
        calculateBowlingScore,
        Car,
        fizzBuzz,
        printKeyValues,
        propertyValueAt,
        getHypotenuseLength,
        capitalize,
        summation,
        sumNested,
        wordCount
    };
}
else {
    const possibleExports = {};
    if (typeof(analyzePrices) !== "undefined") {
        possibleExports.analyzePrices = analyzePrices;
    }
    if (typeof(areTheseAnagrams) !== "undefined") {
        possibleExports.areTheseAnagrams = areTheseAnagrams;
    }
    if (typeof(calculateBowlingScore) !== "undefined") {
        possibleExports.calculateBowlingScore = calculateBowlingScore;
    }
    if (typeof(Car) !== "undefined") {
        possibleExports.Car = Car;
    }
    if (typeof(summation) !== "undefined") {
        possibleExports.summation = summation;
    }
    if (typeof(printKeyValues) !== "undefined") {
        possibleExports.printKeyValues = printKeyValues;
    }
    if (typeof(propertyValueAt) !== "undefined") {
        possibleExports.propertyValueAt = propertyValueAt;
    }
    if (typeof(getHypotenuseLength) !== "undefined") {
        possibleExports.getHypotenuseLength = getHypotenuseLength;
    }
    if (typeof(wordCount) !== "undefined") {
        possibleExports.wordCount = wordCount;
    }
    if (typeof(capitalize) !== "undefined") {
        possibleExports.capitalize = capitalize;
    }
    if (typeof(fizzBuzz) !== "undefined") {
        possibleExports.fizzBuzz = fizzBuzz;
    }
    if (typeof(sumNested) !== "undefined") {
        possibleExports.sumNested = sumNested;
    }
    module.exports = possibleExports;
}
