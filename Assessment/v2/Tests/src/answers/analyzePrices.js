module.exports = function analyzePrices(prices, log = false) {
  if (log) console.log("Analysing prices: " + prices);

  let options = [];
  for (let buyIndex = 0; buyIndex < prices.length - 1; ++buyIndex) {
      let potentialBuy = prices[buyIndex];
      let maxDelta = null;
      let maxSellIndex = null;
      for (let sellIndex = buyIndex + 1; sellIndex < prices.length; ++sellIndex) {
        let potentialSell = prices[sellIndex];
        let delta = potentialSell - potentialBuy;

        if (delta > maxDelta) {
          maxDelta = delta;
          maxSellIndex = sellIndex;
        }
      }
      options.push({
        buyIndex,
        maxSellIndex,
        maxDelta
      });
  }

  if (log) console.log(options);

  let best = options.reduce((best, val) => {
    if (val.maxDelta > best.delta) {
      return {
        buyIndex: val.buyIndex,
        sellIndex: val.maxSellIndex,
        delta: val.maxDelta
      }
    }
    return best;
  }, {buyIndex: null, sellIndex: null, delta: null});

  return {
      buyIndex: best.buyIndex,
      sellIndex: best.sellIndex
  };
};
