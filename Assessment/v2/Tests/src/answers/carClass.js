module.exports = function Car() {
    let speed = 0;

    this.getSpeed = function() {
        return speed;
    };

    this.setSpeed = function(newSpeed) {
        if (newSpeed >= 0) {
            speed = newSpeed;
        }
    };

    this.stop = function() {
        speed = 0;
    };
};
