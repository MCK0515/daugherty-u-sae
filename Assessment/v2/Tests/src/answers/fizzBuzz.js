module.exports = function fizzBuzz(n) {
    if (n <= 0) {
        return "";
    }

    let chunks = [];
    for (let i = 1; i <= n; i++){ //every number between 1 and the number supplied
        let msg = "";
        if (!(i%3))
            msg+="fizz"; //if it's divisible by 3
        if (!(i%5))
            msg+="buzz"; //if it's divisible by 5

        chunks.push(i + msg);
    }
    return chunks.join(", ");
};
