module.exports = function printKeyValues(object) {
    Object.keys(object).forEach(key => {
        console.log(key + ": " + object[key]);
    });
};
