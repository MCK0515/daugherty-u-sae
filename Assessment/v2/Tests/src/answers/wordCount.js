module.exports = function wordCount(inputString) {
    let countableString = inputString.trim().replace(/[ ]{2,}/gi," ");
    const words = countableString.split(" ");
    return words.filter(word => (word !== "")).length;
};
