module.exports = function propertyValueAt(object, pathArray) {

    let traversalObject = object;
    for (let i = 0; i < pathArray.length; ++i) {
        traversalObject = traversalObject[pathArray[i]];
        if (traversalObject === undefined) {
            return undefined;
        }
    }
    return traversalObject;
};
