module.exports = function summation(num1, num2) {

    if (num2 < 0) {
        return 0;
    }
    if (num1 > num2) {
        return 0;
    }

    if (num1 < 0) {
        num1 = 0;
    }

    let total = 0;
    for (let i = num1; i <= num2; ++i) {
        if (i > 0) {
            total += i;
        }
    }

    return total;
};
