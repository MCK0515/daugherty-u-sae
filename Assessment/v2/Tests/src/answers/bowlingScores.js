module.exports = function calculateBowlingScore(scoreString, log = false) {
    if (log) console.log("Starting scoring for: " + scoreString);

    let frame = 1;
    let scores = scoreString.split('');
    let intScores = scores.map(char => {
        if (char === 'X') return 10;
        if (char === '-') return 0;
        if (char === '/') return -1;
        return parseInt(char);
    });

    if (log) console.log("scores: " + scores);
    if (log) console.log("Int Scores: " + intScores);

    return intScores.reduce((sum, val, index, array) => {
        if (log) console.log("frame: " + frame + " " + val);
        if (frame >= 11) {
            // do custom stuff for end of games
            if (log) console.log("Ignoring extra balls for 10th frame");
            return sum;
        }
        else if (val === 10) {
            // strikes
            let next1 = array.length > index + 1 ? array[index+1] : 0;
            let next2 = array.length > index + 2 ? array[index+2] : 0;
            frame += 1;
            return sum + val + next1 + next2;
        }
        else if (val < 0) {
            // spares
            let prev  = array[index-1];
            let next1 = array.length > index + 1 ? array[index+1] : 0;
            frame += 0.5;
            return sum + (10 - prev) + next1;
        }
        else {
            frame += 0.5;
            return sum + val;
        }
    }, 0);
};
