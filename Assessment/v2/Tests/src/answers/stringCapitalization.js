module.exports = function capitalize(str){
    // take first 'word-character' and uppercase it
    return str.replace(/\b./g, (match) => match.toUpperCase());
};
