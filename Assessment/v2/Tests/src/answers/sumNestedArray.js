module.exports = function sumNested(array) {
    return array.reduce((sum, value) => {
        if (typeof value === "number") {  // if the entry is a number directly add it
            return sum + value;
        }
        else if (value instanceof Array) {
            return sum + sumNested(value);
        }
        else {
            return sum;
        }
    }, 0);
};
