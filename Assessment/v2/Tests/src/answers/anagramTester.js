module.exports = function areTheseAnagrams(string1, string2) {
    if (string1.length !== string2.length) {
        return false;
    }

    const sortedString1characters = Array.from(string1).sort().join("");
    const sortedString2characters = Array.from(string2).sort().join("");
    return sortedString1characters === sortedString2characters;
};
