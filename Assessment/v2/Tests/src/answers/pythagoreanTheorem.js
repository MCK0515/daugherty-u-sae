module.exports = function getHypotenuseLength(a, b) {
    if (a <= 0 || b <= 0) {
        return 0;
    }

    return Math.sqrt(a * a + b * b);
};
