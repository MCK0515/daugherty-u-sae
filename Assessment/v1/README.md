# 02 - Assessment

This folder contains the documents used to deliver the code challenges and the short 
answer exam portions of the Daugherty University assessment.

## Code Challenge

The coding challenge portion is a series of requirements that are to be implemented 
in JavaScript functions. 

### Assessment - Coding Challenges.docx

This file contains the written instructions for the candidates that pertain to the 
code challenge part of their assessment.

### Code Challenge Validator & Answer Key

In this folder, you will find a simple Jest unit testing harness for validating the
basic completeness of an applicant's submitted code solutions.

To setup the tool, clone down this repository, and then in the `Code Challenge Validator`
folder run `npm install`.

To ensure that the tests are running correctly, an answer key for the code challenges
is provided in `./src/answer-key.js` and the tests can be run against that by setting
`jest.globals.useTestAnswers` to true in the `./package.json` file for the validator.

To validate a candidate's answers, paste the content of their submitted JSFiddle into
 the `./src/answers.js` file where indicated. Then run the tests, either in an IDE or
 by using `npm test`. This will output some automated results that indicate how well
 they followed instructions. Passing tests should be considered a significant portion
 of their code result, but all submissions should be reviewed by hand for quality and
 ingenuity.

## Short Answer

### Assessment - Short Answer.docx

This file contains the written questions in exam form for the short answer part of
their assessment.

### Assessment - Short Answer - Solutions.docx

This provides a sample set of answers for each question, as well as hints for topics
or lines of reasoning that could result in awarding bonus points for that question.
