let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 7", () => {

    it("has a 'fizzBuzz' function", () => {
        expect(Answers.fizzBuzz).toBeDefined();
    });

    it ("handles the 'counter === 0' edge case", () => {
        expect(Answers.fizzBuzz(0)).toBe("");
    });

    it ("handles the 'counter < 0' edge case", () => {
        expect(Answers.fizzBuzz(-1)).toBe("");
    });

    const exampleOutput = "1, 2, 3fizz, 4, 5buzz, 6fizz, 7, 8, 9fizz, 10buzz, 11, 12fizz, 13, 14, 15fizzbuzz";
    it ("works for the example: (15) => " + exampleOutput, () => {
       expect(Answers.fizzBuzz(15)).toBe(exampleOutput);
    });

    const output_16 = "1, 2, 3fizz, 4, 5buzz, 6fizz, 7, 8, 9fizz, 10buzz, 11, 12fizz, 13, 14, 15fizzbuzz, 16";
    it ("works for 16: (16) => " + output_16, () => {
        expect(Answers.fizzBuzz(16)).toBe(output_16);
    });

    const output_2 = "1, 2";
    it ("works for 2: (2) => " + output_2, () => {
        expect(Answers.fizzBuzz(2)).toBe(output_2);
    });
});
