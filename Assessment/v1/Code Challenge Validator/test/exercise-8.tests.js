let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 8", () => {

    it ("has a 'sumNested' function", () => {
        expect(Answers.sumNested).toBeDefined();
    });

    it ("works for the empty array edge case", () => {
        expect(Answers.sumNested([])).toBe(0);
    });

    it ("works for the example: ([1, 1, 1, [3, 4, [8]], [5]] => 23", () => {
       expect(Answers.sumNested([1, 1, 1, [3, 4, [8]], [5]])).toBe(23);
    });

    it ("handles nested empty arrays: [1, 2, 3, [], [5, 6, []], [[[]]]] => 17", () => {
        expect(Answers.sumNested([1, 2, 3, [], [5, 6, []], [[[]]]])).toBe(17);
    });
});
