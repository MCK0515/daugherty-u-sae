let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 5", () => {

    it("has a 'wordCount' function", () => {
        expect(Answers.wordCount).toBeDefined();
    });

    it ("works for the first example: This is a short sentence! => 5", () => {
        expect(Answers.wordCount("This is a short sentence!")).toBe(5);
    });

    it ("works for the second example: ThisIsA!$ReallyLongWord => 1", () => {
       expect(Answers.wordCount("ThisIsA!$ReallyLongWord")).toBe(1);
    });

    it ("works for the third example: whitespace => 0", () => {
        expect(Answers.wordCount("          ")).toBe(0);
    });

    it ("works for multiple spaces between words", () => {
        expect(Answers.wordCount("This   test   has   lots -of-   spaces.")).toBe(6);
    });

    it ("works for one word", () => {
        expect(Answers.wordCount("one-word")).toBe(1);
    });

    it ("works for leading whitespace", () => {
       expect(Answers.wordCount("  two words")).toBe(2);
    });

    it ("works for trailing whitespace", () => {
        expect(Answers.wordCount("three -new- words   ")).toBe(3);
    });
});
