let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 9", () => {

    it ("has an 'areTheseAnagrams' function", () => {
        expect(Answers.areTheseAnagrams).toBeDefined();
    });

    it ("works for example 1: (abc, bca) => true", () => {
        expect(Answers.areTheseAnagrams("abc", "bca")).toBeTruthy();
    });

    it ("works for example 2: (abc, cde) => false", () => {
        expect(Answers.areTheseAnagrams("abc", "cde")).toBeFalsy();
    });

    it ("works for different lengths of the same characters: (abc, aabbcc) => false", () => {
       expect(Answers.areTheseAnagrams("abc", "aabbcc")).toBeFalsy();
    });

    it ("works for identical strings: (abc, abc) => true", () => {
        expect(Answers.areTheseAnagrams("abc", "abc")).toBeTruthy();
    });
});
