let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 6", () => {

    it("has a 'capitalize' function", () => {
        expect(Answers.capitalize).toBeDefined();
    });

    it("works for the example: i love to code! => I Love To Code!", () => {
       expect(Answers.capitalize("i love to code!")).toBe("I Love To Code!");
    });

    it ("works for the empty string", () => {
        expect(Answers.capitalize("")).toBe("");
    });

    it ("works for leading non-alpha characters: >something => >Something", () => {
       expect(Answers.capitalize(">something")).toBe(">Something");
    });

    it ("works for 'words' with no alpha characters: >!12345!< => >!12345!<", () => {
       expect(Answers.capitalize(">!12345!<")).toBe(">!12345!<");
    });

    it ("works for sentences with bad whitespacing: this  sentence   has long   spaces => This  Sentence   Has Long   Spaces", () => {
        expect(Answers.capitalize("this  sentence   has long   spaces")).toBe("This  Sentence   Has Long   Spaces");
    });
});
