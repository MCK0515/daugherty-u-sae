let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 1", () => {

    it("has a summation function", () => {
        expect(Answers.summation).toBeDefined();
    });

    it("works for the first example: (4, 10) => 49", () => {
       expect(Answers.summation(4, 10)).toBe(49);
    });

    it("works for the second example: (10, 4) => 0", () => {
        expect(Answers.summation(10, 4)).toBe(0);
    });

    it("works when the numbers match: (4, 4) => 4", () => {
       expect(Answers.summation(4, 4)).toBe(4);
    });

    it("works for starting at zero: (0, 10) => 55", () => {
        expect(Answers.summation(0, 10)).toBe(55);
    });

    it("BONUS: works for negative numbers: (-5, 0) => 0", () => {
        expect(Answers.summation(-5, 0)).toBe(0);
    });

    it("BONUS: works for negative numbers: (-5, 5) => 15", () => {
        expect(Answers.summation(-5, 5)).toBe(15);
    });

});
