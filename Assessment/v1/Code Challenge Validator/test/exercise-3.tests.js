let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 3", () => {

    it("has a 'getHypotenuseLength' function", () => {
        expect(Answers.getHypotenuseLength).toBeDefined();
    });

    it("works for the example: (3, 4) => 5", () => {
        expect(Answers.getHypotenuseLength(3, 4)).toBe(5);
    });

    it("handles the a = 0 edge case", () => {
        expect(Answers.getHypotenuseLength(0, 4)).toBe(0);
    });

    it("handles the a < 0 edge case", () => {
        expect(Answers.getHypotenuseLength(-1, 4)).toBe(0);
    });

    it("handles the b = 0 edge case", () => {
        expect(Answers.getHypotenuseLength(3, 0)).toBe(0);
    });

    it("handles the b < 0 edge case", () => {
        expect(Answers.getHypotenuseLength(3, -1)).toBe(0);
    });

    it("works for (5, 5) => ~7.0710678", () => {
        expect(Answers.getHypotenuseLength(5, 5)).toBeCloseTo(7.0710678118654755);
    });


});
