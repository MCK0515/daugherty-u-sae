let Answers = null;
if (useTestAnswers) {
    Answers = require("../src/answer-key.js");
}
else {
    Answers = require("../src/answers.js");
}

describe("Exercise 4", () => {

    it("has a 'Car' function", () => {
        expect(Answers.Car).toBeDefined();
    });

    it("Car function can be used to create an object with the right prototype", () => {
       const car = new Answers.Car();
       expect(car).toBeDefined();
       expect(car.getSpeed).toBeDefined();
       expect(car.setSpeed).toBeDefined();
       expect(car.stop).toBeDefined();
       expect(typeof car.getSpeed).toBe("function");
       expect(typeof car.setSpeed).toBe("function");
       expect(typeof car.stop).toBe("function");
    });

    it("Works for the example", () => {
        const car = new Answers.Car();
        expect(car.getSpeed()).toBe(0);
        car.setSpeed(10);
        expect(car.getSpeed()).toBe(10);
        car.stop();
        expect(car.getSpeed()).toBe(0);
    });

    it("Works if you call things in a different order than the example", () => {
        const car = new Answers.Car();
        expect(car.getSpeed()).toBe(0);
        car.stop();
        expect(car.getSpeed()).toBe(0);
        car.setSpeed(10);
        expect(car.getSpeed()).toBe(10);
    });

    it("handles the negative setSpeed edge case", () => {
        const car = new Answers.Car();
        expect(car.getSpeed()).toBe(0);
        car.setSpeed(10);
        expect(car.getSpeed()).toBe(10);
        car.setSpeed(-1);
        expect(car.getSpeed()).toBe(10);
        car.setSpeed(0);
        expect(car.getSpeed()).toBe(0);
    });

});
