///////////////////////////////
// PASTE CODE BELOW HERE
///////////////////////////////





///////////////////////////////
// DO NOT EDIT BELOW THIS LINE
///////////////////////////////
const possibleExports = {};
if (typeof(summation) !== "undefined") {
    possibleExports.summation = summation;
}
if (typeof(printKeyValues) !== "undefined") {
    possibleExports.printKeyValues = printKeyValues;
}
if (typeof(getHypotenuseLength) !== "undefined") {
    possibleExports.getHypotenuseLength = getHypotenuseLength;
}
if (typeof(Car) !== "undefined") {
    possibleExports.Car = Car;
}
if (typeof(wordCount) !== "undefined") {
    possibleExports.wordCount = wordCount;
}
if (typeof(capitalize) !== "undefined") {
    possibleExports.capitalize = capitalize;
}
if (typeof(fizzBuzz) !== "undefined") {
    possibleExports.fizzBuzz = fizzBuzz;
}
if (typeof(sumNested) !== "undefined") {
    possibleExports.sumNested = sumNested;
}
if (typeof(areTheseAnagrams) !== "undefined") {
    possibleExports.areTheseAnagrams = areTheseAnagrams;
}
module.exports = possibleExports;
