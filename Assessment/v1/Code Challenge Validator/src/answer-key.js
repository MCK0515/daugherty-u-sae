function summation(num1, num2) {

    let total = 0;
    for (let i = num1; i <= num2; ++i) {
        if (i > 0) {
            total += i;
        }
    }

    return total;
}

function printKeyValues(object) {
    Object.keys(object).forEach(key => {
        console.log(key + ": " + object[key]);
    });
}

function getHypotenuseLength(a, b) {
    if (a <= 0 || b <= 0) {
        return 0;
    }

    return Math.sqrt(a * a + b * b);
}

function Car() {
    let speed = 0; //private variable to prevent unwanted fiddling

    this.getSpeed = function() {
        return speed;
    };

    this.setSpeed = function(newSpeed) {
        if (newSpeed < 0) {
            return;
        }

        speed = newSpeed;
    };

    this.stop = function() {
        speed = 0;
    };
}

function wordCount(inputString) {
    let countableString = inputString.trim().replace(/[ ]{2,}/gi," ");
    const words = countableString.split(" ");
    return words.filter(word => (word !== "")).length;
}

function capitalize(str){
    // take first 'word-character' and uppercase it
    return str.replace(/\b./g, (match) => match.toUpperCase());
}

function fizzBuzz(n) {
    if (n <= 0) {
        return "";
    }

    let chunks = [];
    for (let i = 1; i <= n; i++){ //every number between 1 and the number supplied
        let msg = "";
        if (!(i%3))
            msg+="fizz"; //if it's divisible by 3
        if (!(i%5))
            msg+="buzz"; //if it's divisible by 5

        chunks.push(i + msg);
    }
    return chunks.join(", ");
}

function sumNested(array) {
    return array.reduce((sum, value) => {
        if (typeof value === "number") {  // if the entry is a number directly add it
            return sum + value;
        }
        else if (value instanceof Array) {
            return sum + sumNested(value);
        }
        else {
            return sum;
        }
    }, 0);
}

function areTheseAnagrams(string1, string2) {
    if (string1.length !== string2.length) {
        return false;
    }

    const sortedString1characters = Array.from(string1).sort().join("");
    const sortedString2characters = Array.from(string2).sort().join("");
    return sortedString1characters === sortedString2characters;
}

module.exports = {
    summation,
    printKeyValues,
    getHypotenuseLength,
    Car,
    wordCount,
    capitalize,
    fizzBuzz,
    sumNested,
    areTheseAnagrams
};
